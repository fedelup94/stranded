﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace AITests
{
    public class IATestSuite
    {
        // A Test behaves as an ordinary method
        //[Test]
        //public void IATestSuiteSimplePasses()
        //{



        //    // Use the Assert class to test conditions
        //}


        [UnityTest]
        public IEnumerator IATestEnemyShoot()
        {

            SceneManager.LoadScene("TESTAIScene");

            yield return new WaitForSeconds(1f);

            GameObject gameEssentials = GameObject.Find("GameEssentials");
            //GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));

            yield return new WaitForSeconds(1f);

            var player = gameEssentials.transform.GetChild(0);

            //player.GetComponent<CharacterController>().enabled = false;

            //player.transform.position = new Vector3(0, 0, 0);

            //var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/LittleDevilRangedPrefab"));
            var enemy = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/LittleDevilRangedPrefab"));
            enemy.transform.position = new Vector3(8, 0.1f, 5);

            enemy.GetComponent<NavMeshAgent>().enabled = false;
            enemy.GetComponent<NavMeshAgent>().enabled = true;

            float oldHP = gameEssentials.transform.GetChild(8).GetComponentInChildren<EnergyManager>().Amount(EnergyType.Health);


            yield return new WaitForSeconds(4f);


            float newHP = gameEssentials.transform.GetChild(8).GetComponentInChildren<EnergyManager>().Amount(EnergyType.Health);

            //Assert.IsNotNull(gameEssentials.transform.GetChild(8));

            Assert.Less(newHP, oldHP);


        }

        [UnityTest]
        public IEnumerator IATestEnemyMeleeAttack()
        {

            SceneManager.LoadScene("TESTAIScene");

            yield return new WaitForSeconds(1f);

            GameObject gameEssentials = GameObject.Find("GameEssentials");
            //GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));

            yield return new WaitForSeconds(1f);

            var player = gameEssentials.transform.GetChild(0);


            var enemy = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/LittleDevilMeleePrefab"));
            enemy.transform.position = new Vector3(0, 0.1f, 5);

            enemy.GetComponent<NavMeshAgent>().enabled = false;
            enemy.GetComponent<NavMeshAgent>().enabled = true;



            float oldHP = gameEssentials.transform.GetChild(8).GetComponentInChildren<EnergyManager>().Amount(EnergyType.Health);


            yield return new WaitForSeconds(3f);


            float newHP = gameEssentials.transform.GetChild(8).GetComponentInChildren<EnergyManager>().Amount(EnergyType.Health);

            Assert.Less(newHP, oldHP);
        }

        [UnityTest]
        public IEnumerator IATestChasePlayer()
        {

            SceneManager.LoadScene("TESTAIScene");

            yield return new WaitForSeconds(1f);

            GameObject gameEssentials = GameObject.Find("GameEssentials");
            //GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));

            yield return new WaitForSeconds(1f);

            var player = gameEssentials.transform.GetChild(0);

            //player.GetComponent<CharacterController>().enabled = false;

            //player.transform.position = new Vector3(0, 0, 0);

            //var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/LittleDevilRangedPrefab"));
            var enemy = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/LittleDevilMeleePrefab"));

            enemy.GetComponent<IA.Monster>().enabled = false;
            enemy.GetComponent<IA.Monster>().AggroRadius = 30;
            enemy.GetComponent<IA.Monster>().enabled = true;

            var enemyStartingPosition = new Vector3(8, 0.1f, 15);
            enemy.transform.position = enemyStartingPosition;

            var oldDistanceBetweenPlayerAndEnemy = Vector3.Distance(enemy.transform.position, player.transform.position);

            enemy.GetComponent<NavMeshAgent>().enabled = false;
            enemy.GetComponent<NavMeshAgent>().enabled = true;

            yield return new WaitForSeconds(3f);

            var newDistanceBetweenPlayerAndEnemy = Vector3.Distance(enemy.transform.position, player.transform.position);



            //Assert.IsNotNull(gameEssentials.transform.GetChild(8));

            Assert.Less(newDistanceBetweenPlayerAndEnemy, oldDistanceBetweenPlayerAndEnemy);


            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }

        [UnityTest]
        public IEnumerator IATestChaseShip()
        {

            SceneManager.LoadScene("TESTAIScene");

            yield return new WaitForSeconds(1f);

            GameObject gameEssentials = GameObject.Find("GameEssentials");

            yield return new WaitForSeconds(1f);

            var ship = gameEssentials.transform.GetChild(2);

            var enemy = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/LittleDevilMeleePrefab"));

            enemy.GetComponent<IA.Monster>().enabled = false;
            enemy.GetComponent<IA.Monster>().AggroRadius = 3;
            enemy.GetComponent<IA.Monster>().enabled = true;

            var enemyStartingPosition = new Vector3(25, 0.1f, 25);
            enemy.transform.position = enemyStartingPosition;

            var oldDistanceBetweenShipAndEnemy = Vector3.Distance(enemy.transform.position, ship.transform.position);

            enemy.GetComponent<NavMeshAgent>().enabled = false;
            enemy.GetComponent<NavMeshAgent>().enabled = true;

            yield return new WaitForSeconds(3f);

            var newDistanceBetweenShipAndEnemy = Vector3.Distance(enemy.transform.position, ship.transform.position);



            //Assert.IsNotNull(gameEssentials.transform.GetChild(8));

            Assert.Less(newDistanceBetweenShipAndEnemy, oldDistanceBetweenShipAndEnemy);


            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }

        [UnityTest]
        public IEnumerator IATestAttackShip()
        {

            SceneManager.LoadScene("TESTAIScene");

            yield return new WaitForSeconds(1f);

            GameObject gameEssentials = GameObject.Find("GameEssentials");

            yield return new WaitForSeconds(1f);

            var player = gameEssentials.transform.GetChild(0);
            var enemy = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/LittleDevilMeleePrefab"));

            gameEssentials.transform.GetChild(8).GetComponentInChildren<ShipEnergyManager>().LoseEnergy(EnergyType.Laser, 500);

            var oldShipHP = gameEssentials.transform.GetChild(8).GetComponentInChildren<ShipEnergyManager>().Amount(EnergyType.Health);

            enemy.GetComponent<IA.Monster>().enabled = false;
            enemy.GetComponent<IA.Monster>().AggroRadius = 3;
            enemy.GetComponent<IA.Monster>().enabled = true;

            var enemyStartingPosition = new Vector3(17, 0.1f, 17);
            enemy.transform.position = enemyStartingPosition;


            enemy.GetComponent<NavMeshAgent>().enabled = false;
            enemy.GetComponent<NavMeshAgent>().enabled = true;

            yield return new WaitForSeconds(5f);
            var newShipHP = gameEssentials.transform.GetChild(8).GetComponentInChildren<ShipEnergyManager>().Amount(EnergyType.Health);


            Assert.Less(newShipHP, oldShipHP);


            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
        }

    }
}
