﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.AI;

public class ToolsTestSuite
{ 
    [UnityTest]
    public IEnumerator CanStartBuilding()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(.1f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));

        yield return new WaitForSeconds(.1f);
        GameObject buildArea = gameEssentials.transform.GetChild(1).gameObject;
        Assert.AreEqual(false, buildArea.activeSelf);
        ToggleController toggle = gameEssentials.transform.GetChild(6).transform.GetChild(5).GetComponent<ToggleController>();
        PressableButton buildButton = gameEssentials.transform.GetChild(6).transform.GetChild(6).transform.GetChild(7).GetComponent<PressableButton>();
        toggle.Toggle();
        yield return new WaitForSeconds(.1f);
        //L'area di costruzione si è attivata al toggle del pulsante
        Assert.AreEqual(true, buildArea.activeSelf);
        //Tuttavia non ha ancora nessuno strumento da costruire
        Assert.Throws<UnityEngine.UnityException>(() => buildArea.transform.GetChild(1));
        //E difatti il pulsante di costruzione non è visibile
        Assert.AreEqual(false, buildButton.gameObject.activeSelf);
        BuildableTool turret = gameEssentials.transform.GetChild(6).transform.GetChild(6).transform.GetChild(4).transform.GetChild(0).GetComponent<BuildableTool>();
        turret.ToggleBuildButton();
        yield return new WaitForSeconds(.1f);
        //Adesso ha un oggetto da costruire attivo
        Assert.AreNotEqual(null, buildArea.gameObject.transform.GetChild(1));
        //Ed è visibile il pulsante di costruzione
        Assert.AreEqual(true, buildButton.gameObject.activeSelf);
        buildButton.OnPointerDown(null);
        yield return new WaitForSeconds(.1f);
        //Il Giocatore però non può subito costruire (l'oggetto da costruire è ancora ancorato alla build area)
        Assert.AreNotEqual(null, buildArea.gameObject.transform.GetChild(1));
        //Sposto in avanti la buildArea
        buildArea.transform.position += new Vector3(0.0f, 0.0f, 5.0f);
        yield return new WaitForSeconds(.1f);
        //Piazzo la torretta
        buildButton.OnPointerDown(null);
        yield return new WaitForSeconds(.1f);
        //Ora la buildArea non dovrebbe avere un secondo figlio
        Assert.Throws<UnityEngine.UnityException>(() => buildArea.transform.GetChild(1));
    }

    [UnityTest]
    public IEnumerator DoesMineExplode()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(.1f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));

        yield return new WaitForSeconds(.1f);
        GameObject buildArea = gameEssentials.transform.GetChild(1).gameObject;
        Assert.AreEqual(false, buildArea.activeSelf);
        ToggleController toggle = gameEssentials.transform.GetChild(6).transform.GetChild(5).GetComponent<ToggleController>();
        PressableButton buildButton = gameEssentials.transform.GetChild(6).transform.GetChild(6).transform.GetChild(7).GetComponent<PressableButton>();
        toggle.Toggle();
        yield return new WaitForSeconds(.1f);
        //L'area di costruzione si è attivata al toggle del pulsante
        Assert.AreEqual(true, buildArea.activeSelf);
        //Tuttavia non ha ancora nessuno strumento da costruire
        Assert.Throws<UnityEngine.UnityException>(() => buildArea.transform.GetChild(1));
        //E difatti il pulsante di costruzione non è visibile
        Assert.AreEqual(false, buildButton.gameObject.activeSelf);
        BuildableTool mine = gameEssentials.transform.GetChild(6).transform.GetChild(6).transform.GetChild(3).transform.GetChild(0).GetComponent<BuildableTool>();
        mine.ToggleBuildButton();
        yield return new WaitForSeconds(.1f);
        //Adesso ha un oggetto da costruire attivo
        Assert.AreNotEqual(null, buildArea.gameObject.transform.GetChild(1));
        //Ed è visibile il pulsante di costruzione
        Assert.AreEqual(true, buildButton.gameObject.activeSelf);
        buildButton.OnPointerDown(null);
        yield return new WaitForSeconds(.1f);
        //Il Giocatore però non può subito costruire (l'oggetto da costruire è ancora ancorato alla build area)
        Assert.AreNotEqual(null, buildArea.gameObject.transform.GetChild(1));
        GameObject objToBuild = buildArea.gameObject.transform.GetChild(1).gameObject;
        //Sposto in avanti la buildArea
        buildArea.transform.position += new Vector3(0.0f, 0.0f, 5.0f);
        yield return new WaitForSeconds(.1f);
        //Piazzo la torretta
        buildButton.OnPointerDown(null);
        yield return new WaitForSeconds(.1f);
        //Ora la buildArea non dovrebbe avere un secondo figlio
        Assert.Throws<UnityEngine.UnityException>(() => buildArea.transform.GetChild(1));
        GameObject enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/SpiderMeleePrefab"), buildArea.gameObject.transform.position + new Vector3(0.0f, 0.0f, 1.0f), Quaternion.identity);
        IA.Monster spider = enemy.GetComponent<IA.Monster>();
        Assert.AreNotEqual(null, enemy);
        yield return new WaitForSeconds(.1f);
        enemy.GetComponent<NavMeshAgent>().enabled = false;
        enemy.GetComponent<NavMeshAgent>().enabled = true;
        yield return new WaitForSeconds(.1f);
        yield return new WaitForSeconds(5.0f);
        Debug.Log(objToBuild);
        Assert.IsTrue(objToBuild == null);
    }

    [UnityTest]
    public IEnumerator DoesTurretFire()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(.1f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));

        yield return new WaitForSeconds(.1f);
        RifleTurretWeapon ammoTurretWeapon = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/ToolsAndWeapons/A_Prefbs/AmmoTurret"), ScientistController.Instance.gameObject.transform.position + new Vector3(0.0f, 0.0f, 5.0f), Quaternion.identity).GetComponent<RifleTurretWeapon>();
        EnergyManager manager = EnergyManager.Manager;
        float prevEnergyValue = ammoTurretWeapon.CapacityLeft;
        Debug.Log(prevEnergyValue);
        Assert.AreNotEqual(null, ammoTurretWeapon);
        yield return new WaitForSeconds(.1f);
        GameObject enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/SpiderMeleePrefab"), ScientistController.Instance.gameObject.transform.position + new Vector3(0.0f, 0.0f, 6.0f), Quaternion.identity);
        IA.Monster spider = enemy.GetComponent<IA.Monster>();
        Assert.AreNotEqual(null, enemy);
        yield return new WaitForSeconds(.1f);
        enemy.GetComponent<NavMeshAgent>().enabled = false;
        enemy.GetComponent<NavMeshAgent>().enabled = true;
        yield return new WaitForSeconds(.1f);
        yield return new WaitForSeconds(5.0f);
        float curEnergyValue = ammoTurretWeapon.CapacityLeft;
        Debug.Log(curEnergyValue);
        Assert.Less(curEnergyValue, prevEnergyValue);
    }

    [UnityTest]
    public IEnumerator DoWallsConnect()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(.1f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));

        yield return new WaitForSeconds(.1f);
        LaserPillar laserPillar1 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/ToolsAndWeapons/A_Prefbs/LaserPillar"), ScientistController.Instance.gameObject.transform.position + new Vector3(0.0f, 0.0f, 5.0f), Quaternion.identity).GetComponent<LaserPillar>();
        Assert.AreEqual(false, laserPillar1.transform.GetChild(3).transform.GetChild(0).gameObject.activeSelf);
        LaserPillar laserPillar2 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/ToolsAndWeapons/A_Prefbs/LaserPillar"), ScientistController.Instance.gameObject.transform.position + new Vector3(2.0f, 0.0f, 5.0f), Quaternion.identity).GetComponent<LaserPillar>();
        Assert.AreEqual(false, laserPillar2.transform.GetChild(3).transform.GetChild(0).gameObject.activeSelf);
        yield return new WaitForSeconds(0.1f);
        Assert.AreEqual(true, laserPillar1.transform.GetChild(3).transform.GetChild(0).gameObject.activeSelf);
        Assert.AreEqual(true, laserPillar2.transform.GetChild(3).transform.GetChild(0).gameObject.activeSelf);

    }



    }