﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.AI;

public class PlayerTestSuite
{
    [UnityTest]
    public IEnumerator DoesNewWaveStart()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(1.0f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));
        ScientistController scientist = gameEssentials.transform.GetChild(0).GetComponent<ScientistController>();
        WavesManager wavesManager = gameEssentials.transform.GetChild(8).transform.GetChild(6).GetComponent<WavesManager>();
        wavesManager.enabled = true;
        ToggleController toggle = gameEssentials.transform.GetChild(6).transform.GetChild(5).GetComponent<ToggleController>();
        wavesManager.CountDownTimer = 2.0f;
        yield return new WaitForSeconds(1.0f);
        Assert.AreEqual(true, wavesManager.HasWaveEnded);
        yield return new WaitForSeconds(2.0f);
        Assert.AreEqual(false, wavesManager.HasWaveEnded);
        Assert.AreEqual(1, wavesManager.ActualWave);
        Assert.AreNotEqual(null, toggle);
        Assert.AreEqual(false, toggle.gameObject.activeSelf);
    }

    [UnityTest]
    public IEnumerator DoesMove()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(0.1f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));
        yield return new WaitForSeconds(0.1f);
        ScientistController scientist = gameEssentials.transform.GetChild(0).GetComponent<ScientistController>();
        VariableJoystick moveJoystick = gameEssentials.transform.GetChild(6).transform.GetChild(2).transform.GetChild(3).GetComponent<VariableJoystick>();
        Vector3 playerPrevPos = scientist.transform.position;
        float movingTime = 0.0f;
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        while (movingTime <= 2.0f)
        {
            eventData.position = new Vector2(200f, 350f);
            moveJoystick.OnDrag(eventData);
            movingTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Vector3 playerCurPos = scientist.transform.position;
        Assert.Less(playerPrevPos.z, playerCurPos.z);

    }

    [UnityTest]
    public IEnumerator DoesPickUpWeapon()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(1.0f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));
        ScientistController scientist = gameEssentials.transform.GetChild(0).GetComponent<ScientistController>();
        VariableJoystick aimJoystick = gameEssentials.transform.GetChild(6).transform.GetChild(2).transform.GetChild(4).GetComponent<VariableJoystick>();
        yield return new WaitForSeconds(1.0f);
        Assert.AreNotEqual(null, aimJoystick);
        yield return new WaitForSeconds(1.0f);
        AmmoRifle rifle = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/ToolsAndWeapons/A_Prefbs/SimpleRifleLVL1")).GetComponent<AmmoRifle>();
        Assert.AreNotEqual(null, rifle);
        scientist.PickWeapon(rifle);
        yield return new WaitForSeconds(1.0f);
        Assert.AreNotEqual(null, scientist.HandHolster);
        scientist.SwitchWeapon();
        yield return new WaitForSeconds(2.0f);
        Assert.AreNotEqual(null, scientist.BackHolster);
    }

    [UnityTest]
    public IEnumerator DoesFireWeapon()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(1.0f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));
        ScientistController scientist = gameEssentials.transform.GetChild(0).GetComponent<ScientistController>();
        VariableJoystick aimJoystick = gameEssentials.transform.GetChild(6).transform.GetChild(2).transform.GetChild(4).GetComponent<VariableJoystick>();
        yield return new WaitForSeconds(1.0f);
        Assert.AreNotEqual(null, aimJoystick);
        yield return new WaitForSeconds(1.0f);
        AmmoRifle rifle = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/ToolsAndWeapons/A_Prefbs/SimpleRifleLVL1")).GetComponent<AmmoRifle>();
        Assert.AreNotEqual(null, rifle);
        scientist.PickWeapon(rifle);
        yield return new WaitForSeconds(1.0f);
        Assert.AreNotEqual(null, scientist.HandHolster);
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        float firingTime = 0.0f;
        while(firingTime <= 2.0f)
        {
            eventData.position = new Vector2(1700f, 350f);
            aimJoystick.OnDrag(eventData);
            firingTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2.0f);
    }

    [UnityTest]
    public IEnumerator DoesKillEnemy()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(.1f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));
        yield return new WaitForSeconds(0.1f);
        WavesManager wavesManager = gameEssentials.transform.GetChild(8).transform.GetChild(6).GetComponent<WavesManager>();
        wavesManager.enabled = false;
        ScientistController scientist = gameEssentials.transform.GetChild(0).GetComponent<ScientistController>();
        VariableJoystick aimJoystick = gameEssentials.transform.GetChild(6).transform.GetChild(2).transform.GetChild(4).GetComponent<VariableJoystick>();
        yield return new WaitForSeconds(0.1f);
        Assert.AreNotEqual(null, aimJoystick);
        yield return new WaitForSeconds(0.1f);

        GameObject enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/SpiderMeleePrefab"), scientist.transform.position + new Vector3(0.0f, 0.0f, 5.0f), Quaternion.identity);
        IA.Monster spider = enemy.GetComponent<IA.Monster>();
        Assert.AreNotEqual(null, enemy);
        enemy.GetComponent<NavMeshAgent>().enabled = false;
        enemy.GetComponent<NavMeshAgent>().enabled = true;

        yield return new WaitForSeconds(.1f);
        AmmoRifle rifle = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/ToolsAndWeapons/A_Prefbs/SimpleRifleLVL1")).GetComponent<AmmoRifle>();
        Assert.AreNotEqual(null, rifle);
        scientist.PickWeapon(rifle);
        yield return new WaitForSeconds(2.0f);
        Assert.AreNotEqual(null, scientist.HandHolster);
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        float firingTime = 0.0f;
        while (firingTime <= 4.0f)
        {
            eventData.position = new Vector2(1700f, 350f);
            aimJoystick.OnDrag(eventData);
            firingTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Assert.AreNotEqual(null, spider);

        yield return new WaitForSeconds(2.0f);
    }

    [UnityTest]
    public IEnumerator DoesDieAndGameOver()
    {
        SceneManager.LoadScene("TestScene");
        yield return new WaitForSeconds(.1f);
        GameObject gameEssentials = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameEssentials"));
        yield return new WaitForSeconds(0.1f);
        WavesManager wavesManager = gameEssentials.transform.GetChild(8).transform.GetChild(6).GetComponent<WavesManager>();
        wavesManager.enabled = false;
        ScientistController scientist = gameEssentials.transform.GetChild(0).GetComponent<ScientistController>();
        scientist.takeDamage(490f);
        VariableJoystick aimJoystick = gameEssentials.transform.GetChild(6).transform.GetChild(2).transform.GetChild(4).GetComponent<VariableJoystick>();
        yield return new WaitForSeconds(0.1f);
        Assert.AreNotEqual(null, aimJoystick);
        yield return new WaitForSeconds(0.1f);

        GameObject enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/AIPrefabs/SpiderMeleePrefab"), scientist.transform.position + new Vector3(0.0f, 0.0f, 5.0f), Quaternion.identity);
        IA.Monster spider = enemy.GetComponent<IA.Monster>();
        Assert.AreNotEqual(null, enemy);
        enemy.GetComponent<NavMeshAgent>().enabled = false;
        enemy.GetComponent<NavMeshAgent>().enabled = true;
        yield return new WaitForSeconds(5.0f);
        //Controllo che il giocatore sia morto
        Assert.AreEqual(true, scientist.HasDied());
        //Controllo che il pannello di Game Over sia attivo
        Assert.AreEqual(true, gameEssentials.transform.GetChild(5).gameObject.activeSelf);

    }

}
