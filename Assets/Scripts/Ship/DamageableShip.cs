﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageableShip : MonoBehaviour, IDamageable
{
    public bool HasDied()
    {
        return ShipEnergyManager.Manager.Amount(EnergyType.Health) <= 0f;
    }

    public void takeDamage(float damage)
    {
        float damageTaken = ShipEnergyManager.Manager.LoseEnergy(EnergyType.Health, damage);
    }
}
