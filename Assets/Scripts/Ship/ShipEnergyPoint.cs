﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShipEnergyPoint : MonoBehaviour, IRefillable
{
    [SerializeField] private EnergyType energyType;
    [SerializeField] private Slider energySlider;
    [SerializeField] private TextMeshProUGUI text;


    public void Start()
    {
        energySlider.maxValue = ShipEnergyManager.Manager.MaxAmount(this.energyType);
        energySlider.value = ShipEnergyManager.Manager.Amount(this.energyType);
        text.text = ((int)ShipEnergyManager.Manager.Amount(this.energyType)).ToString() + "/" + energySlider.maxValue.ToString();
        EventsManager.Manager.shipEnergyLeft.AddListener(OnEnergyValueChanged);
    }

    public void OnEnergyValueChanged(EnergyType energyType, float energyAmount)
    {
        if (energyType == this.energyType)
        {
            energySlider.value = energyAmount;
            text.text = ((int)ShipEnergyManager.Manager.Amount(this.energyType)).ToString() + "/" + energySlider.maxValue.ToString();
        }
  

    }


    public bool Refill(float energyAmount)
    {
        ShipEnergyManager.Manager.ReceiveEnergy(this.energyType, energyAmount);
        if (ShipEnergyManager.Manager.Amount(energyType) >= ShipEnergyManager.Manager.MaxAmount(this.energyType))
            return true;
        else
            return false;
    }

    public bool IsFull()
    {
        return ShipEnergyManager.Manager.Amount(energyType) >= ShipEnergyManager.Manager.MaxAmount(this.energyType);
    }

    public bool IsEmpty()
    {
        return ShipEnergyManager.Manager.Amount(energyType) == 0f;
    }
}
