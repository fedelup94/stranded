﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEnergyManager : MonoBehaviour
{
    private static ShipEnergyManager manager;

    [SerializeField] private List<Energy> energiesList;
    private Dictionary<EnergyType, float> energiesDict;
    private Dictionary<EnergyType, float> maxEnergiesDict;

    public static ShipEnergyManager Manager { get => manager; }

    private void Awake()
    {
        if (manager == null)
        {
            manager = this;
            energiesDict = new Dictionary<EnergyType, float>();
            maxEnergiesDict = new Dictionary<EnergyType, float>();
            for (int i = 0; i < energiesList.Count; i++)
            {
                energiesDict[energiesList[i].energyType] = energiesList[i].initialAmount;
                maxEnergiesDict[energiesList[i].energyType] = energiesList[i].maxAmount;
            }
        }
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        foreach (KeyValuePair<EnergyType, float> energy in energiesDict)
        {
            EventsManager.Manager.shipEnergyLeft.Invoke(energy.Key, energy.Value);
        }

    }

    public float MaxAmount(EnergyType energyType)
    {
        return maxEnergiesDict[energyType];
    }

    public float Amount(EnergyType energyType)
    {
        return energiesDict[energyType];
    }

    public float LoseEnergy(EnergyType energyType, float energyAmount)
    {
        if (energiesDict[energyType] <= 0f)
            return 0f;

        if (energiesDict[energyType] < energyAmount)
            energyAmount = energiesDict[energyType];
        energiesDict[energyType] -= energyAmount;
        EventsManager.Manager.shipEnergyLeft.Invoke(energyType, energiesDict[energyType]);
        return energyAmount;
    }

    public void ReceiveEnergy(EnergyType energyType, float energyAmount)
    {
        if (energyAmount <= 0)
            return;
        energiesDict[energyType] += energyAmount;
        if (energiesDict[energyType] > maxEnergiesDict[energyType])
            energiesDict[energyType] = maxEnergiesDict[energyType];

        EventsManager.Manager.shipEnergyLeft.Invoke(energyType, energiesDict[energyType]);
    }
}
