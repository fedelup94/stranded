﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEnergyShield : MonoBehaviour, IDamageable
{
    [SerializeField] private Color maxHealthColor;
    [SerializeField] private Color minHealthColor;
    private Renderer renderer;

    private void Awake()
    {
        renderer = GetComponent<Renderer>();
    }

    private void Start()
    {

        Color lerpColor = Color.Lerp(maxHealthColor, minHealthColor, (ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser) - ShipEnergyManager.Manager.Amount(EnergyType.Laser)) / ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser));
        renderer.sharedMaterial.color = lerpColor;
        renderer.sharedMaterial.SetColor("_EmissionColor", lerpColor * 0.5f);
        EventsManager.Manager.shipEnergyLeft.AddListener(OnLaserEnergyValueChanged);
    }

    public void OnLaserEnergyValueChanged(EnergyType energyType, float energyAmount)
    {
        if (energyType == EnergyType.Laser && energyAmount > 0 )
        {
            Color lerpColor = Color.Lerp(maxHealthColor, minHealthColor, (ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser) - ShipEnergyManager.Manager.Amount(EnergyType.Laser)) / ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser));
            renderer.sharedMaterial.color = lerpColor;
            renderer.sharedMaterial.SetColor("_EmissionColor", lerpColor * 0.5f);
        }
    }

    public void takeDamage(float damage)
    {
        ShipEnergyManager.Manager.LoseEnergy(EnergyType.Laser, damage);
        Color lerpColor = Color.Lerp(maxHealthColor, minHealthColor, (ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser) - ShipEnergyManager.Manager.Amount(EnergyType.Laser)) / ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser));
        renderer.sharedMaterial.color = lerpColor;
        renderer.sharedMaterial.SetColor("_EmissionColor", lerpColor * 0.5f);
        if (ShipEnergyManager.Manager.Amount(EnergyType.Laser) <= 0f)
            this.gameObject.SetActive(false);
    }

    public bool HasDied()
    {
        return ShipEnergyManager.Manager.Amount(EnergyType.Laser) <= 0f;
    }

    public void Restart()
    {
        Color lerpColor = Color.Lerp(maxHealthColor, minHealthColor, (ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser) - ShipEnergyManager.Manager.Amount(EnergyType.Laser)) / ShipEnergyManager.Manager.MaxAmount(EnergyType.Laser));
        renderer.sharedMaterial.color = lerpColor;
        renderer.sharedMaterial.SetColor("_EmissionColor", lerpColor * 0.5f);
    }
}