﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartEnergyShield : MonoBehaviour
{
    [SerializeField] private ShipEnergyShield shipEnergyShield;
    // Start is called before the first frame update
    void Start()
    {
        EventsManager.Manager.shipEnergyLeft.AddListener(OnLaserEnergyValueChanged);
    }

    public void OnLaserEnergyValueChanged(EnergyType energyType, float energyAmount)
    {
        if(energyType == EnergyType.Laser && energyAmount > 0 && !shipEnergyShield.gameObject.activeInHierarchy)
        {
            shipEnergyShield.gameObject.SetActive(true);
            shipEnergyShield.Restart();
        }
    }

    
}
