﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsPooler : MonoBehaviour
{
    public static BulletsPooler SharedInstance;
    [SerializeField] private List<Projectile> pooledBullets;
    [SerializeField] private List<AIProjectile> pooledEnemyBullets;
    [SerializeField] private List<ResearchProjectile> pooledResearchBullets;
    [SerializeField] private GameObject bulletToPool;
    [SerializeField] private GameObject enemyBulletToPool;
    [SerializeField] private GameObject researchBulletToPool;
    [SerializeField] private int amountToPool;
    [SerializeField] private int amountEnemyToPool;
    [SerializeField] private int amountResearchToPool;

    public GameObject EnemyBulletToPool { get => enemyBulletToPool; private set => enemyBulletToPool = value; }

    private void Awake()
    {
        if (SharedInstance == null)
            SharedInstance = this;
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        //for(int i = 0; i < amountToPool; i++)
        //{
        //    Projectile obj = Instantiate(bulletToPool).GetComponent<Projectile>();
        //    obj.gameObject.SetActive(false);
        //    obj.transform.parent = this.gameObject.transform;
        //    pooledBullets.Add(obj);
        //}

        StartCoroutine(InstantiateBullets());

        //for (int i = 0; i < amountEnemyToPool; i++)
        //{
        //    AIProjectile obj = Instantiate(enemyBulletToPool).GetComponent<AIProjectile>();
        //    obj.gameObject.SetActive(false);
        //    obj.transform.parent = this.gameObject.transform;
        //    pooledEnemyBullets.Add(obj);
        //}

        //for (int i = 0; i < amountResearchToPool; i++)
        //{
        //    ResearchProjectile obj = Instantiate(researchBulletToPool).GetComponent<ResearchProjectile>();
        //    obj.gameObject.SetActive(false);
        //    obj.transform.parent = this.gameObject.transform;
        //    pooledResearchBullets.Add(obj);
        //}

    }


    private IEnumerator InstantiateBullets()
    {
        int k = 4;
        while (pooledBullets.Count < amountToPool)
        {
            for (int i = 0; i < amountToPool; i += k)
            {
                for (int y = 0; y < k; y++)
                {
                    Projectile obj = Instantiate(bulletToPool).GetComponent<Projectile>();
                    obj.gameObject.SetActive(false);
                    obj.transform.parent = this.gameObject.transform;
                    pooledBullets.Add(obj);
                }
                yield return new WaitForEndOfFrame();
            }
        }

        while (pooledEnemyBullets.Count < amountEnemyToPool)
        {
            for (int i = 0; i < amountEnemyToPool; i += k)
            {
                for (int y = 0; y < k; y++)
                {
                    AIProjectile obj = Instantiate(EnemyBulletToPool).GetComponent<AIProjectile>();
                    obj.gameObject.SetActive(false);
                    obj.transform.parent = this.gameObject.transform;
                    pooledEnemyBullets.Add(obj);
                }
                yield return new WaitForEndOfFrame();
            }
        }
        
        while (pooledResearchBullets.Count < amountResearchToPool)
        {
            for (int i = 0; i < amountResearchToPool; i += k)
            {
                for (int y = 0; y < k; y++)
                {
                    ResearchProjectile obj = Instantiate(researchBulletToPool).GetComponent<ResearchProjectile>();
                    obj.gameObject.SetActive(false);
                    obj.transform.parent = this.gameObject.transform;
                    pooledResearchBullets.Add(obj);
                }
                yield return new WaitForEndOfFrame();
            }
        }

    }

    public Projectile GetPooledBullet()
    {
        for(int i = 0; i < pooledBullets.Count; i++)
        {
            if(!pooledBullets[i].gameObject.activeInHierarchy)
            {
                return pooledBullets[i];
            }
        }
        return null;
    }

    public ResearchProjectile GetPooledResearchBullet()
    {
        for (int i = 0; i < pooledResearchBullets.Count; i++)
        {
            if (!pooledResearchBullets[i].gameObject.activeInHierarchy)
            {
                return pooledResearchBullets[i];
            }
        }
        return null;
    }

    public AIProjectile GetEnemyBullet()
    {
        for (int i = 0; i < pooledEnemyBullets.Count; i++)
        {
            if (!pooledEnemyBullets[i].gameObject.activeInHierarchy)
            {
                return pooledEnemyBullets[i];
            }
        }
        return null;
    }
}
