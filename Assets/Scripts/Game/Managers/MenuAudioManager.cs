﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudioManager : MonoBehaviour
{

    [Header("SFX")]
    [SerializeField]
    private AudioSource buttonSFX;

    [SerializeField]
    private AudioSource nextLevelButtonSFX;

    [SerializeField]
    private AudioSource musicBGM;

    private static MenuAudioManager instance;
    

    public static MenuAudioManager Instance { get => instance; }


    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayButtonSFX()
    {
        buttonSFX.Play();
    }

    public void PlayNextLevelButtonSFX()
    {
        nextLevelButtonSFX.Play();
    }

    public void FadeBGM()
    {
        StartCoroutine(MusicFade());
    }

    private IEnumerator MusicFade()
    {
        float fading = musicBGM.volume;
        while (fading > 0)
        {
            fading -= (Time.deltaTime/3);
            musicBGM.volume = fading;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        yield break;
    }
}
