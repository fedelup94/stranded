﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootManager : MonoBehaviour
{

    [SerializeField]
    private Transform bluePotionPrefab;
    [SerializeField]
    private Transform greenPotionPrefab;
    [SerializeField]
    private Transform redPotionPrefab;

    private static LootManager instance;

    public Transform BluePotionPrefab { get => bluePotionPrefab; set => bluePotionPrefab = value; }
    public Transform GreenPotionPrefab { get => greenPotionPrefab; set => greenPotionPrefab = value; }
    public Transform RedPotionPrefab { get => redPotionPrefab; set => redPotionPrefab = value; }
    public static LootManager Instance { get => instance; }

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static GameObject GenerateLootGO(LootObject loot)
    {
        if (loot != null)
        {
            GameObject m_loot = new GameObject("Loot");
            if (loot.RedEnergy != 0)
            {
                var redEnergyGO = Instantiate(LootManager.Instance.RedPotionPrefab.gameObject, m_loot.transform);
                redEnergyGO.transform.localPosition = new Vector3(Random.Range(0f,2f),0,Random.Range(0f,2f));
                redEnergyGO.GetComponent<LootableItem>().EnergyAmount = loot.RedEnergy;
            }
            if (loot.BlueEnergy != 0)
            {
                var blueEnergyGO = Instantiate(LootManager.Instance.BluePotionPrefab.gameObject, m_loot.transform);
                blueEnergyGO.transform.localPosition = new Vector3(Random.Range(0f, 2f), 0, Random.Range(0f, 2f));
                blueEnergyGO.GetComponent<LootableItem>().EnergyAmount = loot.BlueEnergy;
            }
            if (loot.GreenEnergy != 0)
            {
                var greenEnergyGO = Instantiate(LootManager.Instance.GreenPotionPrefab.gameObject, m_loot.transform);
                greenEnergyGO.transform.localPosition = new Vector3(Random.Range(0f, 2f), 0, Random.Range(0f, 2f));
                greenEnergyGO.GetComponent<LootableItem>().EnergyAmount = loot.GreenEnergy;
            }

            if (loot.SpawnableWeapons != null)
            {
                foreach (WeaponSO weapon in loot.SpawnableWeapons)
                {
                    //instantiate weapons
                }
            }


            return m_loot;
        }
        else
            return null;
    }
}
