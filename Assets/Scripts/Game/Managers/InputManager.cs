﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    private static InputManager inputManager;

    [SerializeField] private  VariableJoystick movementJoystick;
    [SerializeField] private  VariableJoystick aimingJoystick;
    [SerializeField] private VariableJoystick buildMovementJoystick;
    [SerializeField] private  PressableButton jumpButton;
    [SerializeField] private PressableButton placeButton;
    [SerializeField] private PressableButton moveButton;
    [SerializeField] private PressableButton refillButton;
    [SerializeField] private Button switchWeaponButton;

    public VariableJoystick MovementJoystick { get => movementJoystick;}
    public VariableJoystick AimingJoystick { get => aimingJoystick;}
    public VariableJoystick BuildMovementJoystick { get => buildMovementJoystick; }
    public PressableButton JumpButton { get => jumpButton;}
    public static InputManager Manager { get => inputManager;}
    public PressableButton PlaceButton { get => placeButton;}
    public PressableButton MoveButton { get => moveButton;}
    public PressableButton RefillButton { get => refillButton;}
    public Button SwitchWeaponButton { get => switchWeaponButton;}

    private void Awake()
    {
        if (inputManager == null)
        {
            inputManager = this;
        }
        else
            Destroy(this.gameObject);
    }


}
