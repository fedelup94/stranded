﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavesManager : MonoBehaviour
{

    [SerializeField]
    private List<WaveObject> waves;
    [SerializeField]
    private List<SpawnPoint> spawnPoints;
    [SerializeField]
    private float countDownTimer = 60f;

    [SerializeField]
    private TMPro.TextMeshProUGUI waveTextComponent;

    private float timerWaveEndedCheckReset = 3.5f;

    private static WavesManager instance;
    [SerializeField]
    private bool hasWaveEnded;
    private float timer;
    private float timerWaveEndedCheck;
    private Dictionary<GameObject, int> maxEnemyPerType;
    private int actualWave;
    private Transform[] m_Waves;

    public static WavesManager Instance { get => instance; }
    public bool HasWaveEnded { get => hasWaveEnded; private set => hasWaveEnded = value; }
    public List<WaveObject> Waves { get => waves; set => waves = value; }
    public List<SpawnPoint> SpawnPoints { get => spawnPoints; set => spawnPoints = value; }
    public float CountDownTimer { get => countDownTimer; set => countDownTimer = value; }
    public float Timer { get => timer; }
    public int ActualWave { get => actualWave; }

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
        }
        actualWave = 0;
        m_Waves = new Transform[Waves.Count];
        maxEnemyPerType = new Dictionary<GameObject, int>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //for (int i = 0; i < Waves.Count; i++)
        //{
        //    GameObject wave = new GameObject("Wave " + i);
        //    wave.transform.SetParent(transform);
        //    wave.transform.localPosition = new Vector3();
        //    foreach (GameObject enemy in Waves[i].EnemiesToSpawn)
        //    {
        //        var newEnemy = Instantiate(enemy, wave.transform, false);
        //        //newEnemy.transform.localPosition = new Vector3(UnityEngine.Random.Range(0f,2f),0, UnityEngine.Random.Range(0f,2f));
        //    }
        //    wave.SetActive(false);
        //    m_Waves[i] = wave.transform;
        //}

        //hasWaveEnded = true;
        //timer = CountDownTimer;
        //Countdown.Instance.StartTimer();
        StartCoroutine(InstantiateWaves());
        timerWaveEndedCheck = timerWaveEndedCheckReset;
    }

    private IEnumerator InstantiateWaves()
    {
        for (int i = 0; i < Waves.Count; i++)
        {
            GameObject wave = new GameObject("Wave " + i);
            wave.transform.SetParent(transform);
            wave.transform.localPosition = new Vector3();
            foreach (GameObject enemy in Waves[i].EnemiesToSpawn)
            {
                var newEnemy = Instantiate(enemy, wave.transform, false);
                //newEnemy.transform.localPosition = new Vector3(UnityEngine.Random.Range(0f,2f),0, UnityEngine.Random.Range(0f,2f));
            }
            wave.SetActive(false);
            m_Waves[i] = wave.transform;
            yield return new WaitForEndOfFrame();
        }

        hasWaveEnded = true;
        timer = CountDownTimer;
        Countdown.Instance.StartTimer();
        waveTextComponent.text = actualWave + "/" + m_Waves.Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (HasWaveEnded)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                StartWave();
            }
        }
        else
        {
            timerWaveEndedCheck -= Time.deltaTime;
            if (timerWaveEndedCheck <= 0)
            {
                CheckActualWaveStatus();
                timerWaveEndedCheck = timerWaveEndedCheckReset;
            }

        }
    }


    public bool CheckActualWaveStatus()
    {
        HasWaveEnded = m_Waves[actualWave-1].childCount == 0;
        if (HasWaveEnded)
        {
            EventsManager.Manager.waveHasEnded.Invoke();
            EnergyManager.Manager.ReceiveEnergy(EnergyType.Money, waves[actualWave - 1].MoneyReceived);
        }
        return HasWaveEnded;
    }

    public void CheckLastWave()
    {
        Debug.Log("AE");
        if (actualWave == m_Waves.Length)
        {
            EventsManager.Manager.noMoreWaves.Invoke();
            Time.timeScale = 0;
        }

    }


    public void StartWave()
    {
        SpawnNextWave();
    }

    private void SpawnNextWave()
    {
        timer = CountDownTimer;
        HasWaveEnded = false;
        Countdown.Instance.InterruptTimer();
        EventsManager.Manager.waveHasStarted.Invoke();
        for(int i = 0; i < m_Waves[actualWave].childCount; i++)
        {
            if (SpawnPoints.Count > 1)
            {
                var spawnPoint = UnityEngine.Random.Range(0, SpawnPoints.Count);
                m_Waves[actualWave].GetChild(i).position = SpawnPoints[spawnPoint].transform.position;                
            } else
            {
                m_Waves[actualWave].GetChild(i).position = SpawnPoints[0].transform.position;
            }
        }
        m_Waves[actualWave].gameObject.SetActive(true);
        actualWave++;
        waveTextComponent.text = actualWave + "/" + m_Waves.Length;
    }

    public void TEST_SetTimer(float f)
    {
        timer = f;
    }
}
