﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class BulletsLeft : UnityEvent<APlayerWeapon, float> { }

[System.Serializable]
public class EnergyLeft : UnityEvent<EnergyType, float> { }

[System.Serializable]
public class WeaponEquipped : UnityEvent<HolsterType, APlayerWeapon> { }

[System.Serializable]
public class PlaceObject : UnityEvent<APlaceable> { }

public class EventsManager : MonoBehaviour
{
    private static EventsManager eventManager;

    private void Awake()
    {
        if (eventManager == null)
        {
            eventManager = this;
        }
        else
            Destroy(this.gameObject);
    }

    public  BulletsLeft magazineBulletsLeft = new BulletsLeft();
    public  BulletsLeft capacityBulletsLeft = new BulletsLeft();
    public  UnityEvent reloadWeapon = new UnityEvent();
    public WeaponEquipped holsterWeaponChanged = new WeaponEquipped();
    public UnityEvent buildMode = new UnityEvent();
    public UnityEvent attackMode = new UnityEvent();
    public PlaceObject startPlaceObject = new PlaceObject();
    public PlaceObject stopPlaceObject = new PlaceObject();
    public PlaceObject hasPlaceObject = new PlaceObject();
    public EnergyLeft energyLeft = new EnergyLeft();
    public EnergyLeft shipEnergyLeft = new EnergyLeft();
    //public UnityEvent playerHasDied = new UnityEvent();
    public UnityEvent waveHasStarted = new UnityEvent();
    public UnityEvent waveHasEnded = new UnityEvent();
    public UnityEvent pause = new UnityEvent();
    public UnityEvent unPause = new UnityEvent();
    public UnityEvent gameOver = new UnityEvent();
    public UnityEvent noMoreWaves = new UnityEvent();

    public static EventsManager Manager { get => eventManager; }


    public void Pause()
    {
        Time.timeScale = 0;
        pause.Invoke();
    }

    public void UnPause()
    {
        Time.timeScale = 1;        
        unPause.Invoke();
    }

    public void OpenMenuScene()
    {
        Time.timeScale = 1;
        SceneLoader.Instance.LoadScene(0);
    }

}
