﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnergyType
{
    Ammo,
    Laser,
    Health,
    Money
}

[System.Serializable]
public struct Energy
{
    public EnergyType energyType;
    public float initialAmount;
    public float maxAmount;
}

public class EnergyManager : MonoBehaviour
{
    private static EnergyManager manager;

    [SerializeField] private List<Energy> energiesList;
    private Dictionary<EnergyType, float> energiesDict;
    private Dictionary<EnergyType, float> maxEnergiesDict;

    public static EnergyManager Manager { get => manager;}

    private void Awake()
    {
        if (manager == null)
        {
            manager = this;
            energiesDict = new Dictionary<EnergyType, float>();
            maxEnergiesDict = new Dictionary<EnergyType, float>();
            for (int i = 0; i < energiesList.Count; i++)
            {
                energiesDict[energiesList[i].energyType] = energiesList[i].initialAmount;
                maxEnergiesDict[energiesList[i].energyType] = energiesList[i].maxAmount;
            }
        }
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        foreach (KeyValuePair<EnergyType, float> energy in energiesDict)
        {
            EventsManager.Manager.energyLeft.Invoke(energy.Key, energy.Value);
        }
            
    }

    public float MaxAmount(EnergyType energyType)
    {
        return maxEnergiesDict[energyType];
    }

    public float Amount(EnergyType energyType)
    {
        return energiesDict[energyType];
    }

    public float LoseEnergy(EnergyType energyType, float energyAmount)
    {
        if (energiesDict[energyType] <= 0f)
            return 0f;

        if(energiesDict[energyType] < energyAmount)
            energyAmount = energiesDict[energyType];
        energiesDict[energyType] -= energyAmount;
        EventsManager.Manager.energyLeft.Invoke(energyType, energiesDict[energyType]);
        return energyAmount;
    }

    public void ReceiveEnergy(EnergyType energyType, float energyAmount)
    {
        if (energyAmount <= 0)
            return;
        energiesDict[energyType] += energyAmount;
        if (energiesDict[energyType] > maxEnergiesDict[energyType])
            energiesDict[energyType] = maxEnergiesDict[energyType];

        EventsManager.Manager.energyLeft.Invoke(energyType, energiesDict[energyType]);
    }
}
