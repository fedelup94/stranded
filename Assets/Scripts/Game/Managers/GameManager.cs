﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager gameManager;
    ScientistController scientist;
    PlaceArea buildArea;
    CameraFollow cameraFollow;

    public static GameManager Manager { get => gameManager;}

    private void Awake()
    {
        if (gameManager == null)
        {
            gameManager = this;
        }
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        scientist = ScientistController.Instance;
        cameraFollow = Camera.main.GetComponent<CameraFollow>();
        buildArea = PlaceArea.Instance;
        SetPlayerMode();

    }

    public void SetBuildMode()
    {
        if (buildArea == null)
            return;
        buildArea.gameObject.transform.position = scientist.gameObject.transform.position;
        buildArea.gameObject.SetActive(true);
        cameraFollow.SetTarget(buildArea.transform);
        Camera.main.orthographicSize = 8;
    }

    public void SetPlayerMode()
    {
        if (buildArea == null)
            return;
        buildArea.gameObject.SetActive(false);
        cameraFollow.SetTarget(scientist.transform);
        Camera.main.orthographicSize = 5.5f;
    }
}
