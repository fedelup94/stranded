﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnergySlider : MonoBehaviour
{
    [SerializeField] EnergyType energyType;
    [SerializeField] Slider slider;
    [SerializeField] TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        slider.maxValue = EnergyManager.Manager.MaxAmount(energyType);
        slider.value = EnergyManager.Manager.Amount(energyType);
        text.text = ((int)slider.value).ToString() + " / " + ((int)slider.maxValue).ToString();
        EventsManager.Manager.energyLeft.AddListener(OnValueChanged);
    }

    public void OnValueChanged(EnergyType energyType, float energyAmount)
    {
        if (energyType == this.energyType)
        {
            slider.value = energyAmount;
            text.text = ((int)slider.value).ToString() + "/" + ((int)slider.maxValue).ToString();
        }

    }
}
