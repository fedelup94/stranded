﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextPanelTrigger : MonoBehaviour
{
    public void TriggerNextPanel()
    {
        TutorialManager.Instance.nextPanelEvent.Invoke();
        Destroy(this.gameObject);
    }
}
