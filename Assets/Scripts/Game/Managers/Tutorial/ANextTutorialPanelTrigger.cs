﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ANextTutorialPanelTrigger : MonoBehaviour
{
    protected void NextTutorialPanelTrigger()
    {
        TutorialManager.Instance.nextPanelEvent.Invoke();
    }
}
