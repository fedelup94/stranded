﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialPanel : ATutorialPanel
{
    public override void OnBeginPanel()
    {
        TutorialManager.Instance.nextPanelEvent.Invoke();
    }

    public override void OnEndPanel()
    {
    }

    public override void OnNextButtonClick()
    {
    }
}
