﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastTutorialPanel : ATutorialPanel
{
    [SerializeField] ToggleController controller;
    public override void OnBeginPanel()
    {
        controller.Toggle();
        ScientistController.Instance.GetComponent<ScientistController>().enabled = true;
    }

    public override void OnEndPanel()
    {
    }

    public override void OnNextButtonClick()
    {
        this.gameObject.SetActive(false);
    }
}
