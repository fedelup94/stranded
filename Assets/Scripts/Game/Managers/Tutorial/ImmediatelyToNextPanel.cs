﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmediatelyToNextPanel : ATutorialPanel
{
    public override void OnBeginPanel()
    {
    }

    public override void OnEndPanel()
    {

    }

    public override void OnNextButtonClick()
    {
        TutorialManager.Instance.nextPanelEvent.Invoke();
    }
}
