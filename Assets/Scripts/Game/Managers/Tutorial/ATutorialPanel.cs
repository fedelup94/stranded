﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ATutorialPanel : MonoBehaviour
{
    [SerializeField] protected List<GameObject> objectsToShow;
    [SerializeField] protected List<GameObject> objectsToHide;
    // Start is called before the first frame update

    public abstract void OnBeginPanel();
    public void BeginPanel()
    {
        for (int i = 0; i < objectsToShow.Count; i++)
        {
            objectsToShow[i].SetActive(true);
        }

        for (int i = 0; i < objectsToHide.Count; i++)
        {
            objectsToHide[i].SetActive(false);
        }
        OnBeginPanel();
    }

    public void EndPanel()
    {
        OnEndPanel();
    }
    public abstract void OnNextButtonClick();
    public abstract void OnEndPanel();
}
