﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSkipButton : MonoBehaviour
{
    private ATutorialPanel panel;
    public void Awake()
    {
        panel = this.transform.parent.GetComponent<ATutorialPanel>();
    }
    public void OnClick()
    {
        panel.OnNextButtonClick();
    }
}
