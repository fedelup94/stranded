﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnGetEnergyTrigger : MonoBehaviour
{
    [SerializeField] ToggleController controller;
    private int energiesCollected = 0;
    private void Start()
    {
        EventsManager.Manager.energyLeft.AddListener(OnEnergyChange);
    }
    public void OnEnergyChange(EnergyType energyType, float amount)
    {
        energiesCollected++;
        if(energiesCollected >= 3)
        {
            TutorialManager.Instance.nextPanelEvent.Invoke();
            controller.Toggle();
            ScientistController.Instance.Stop();
            ScientistController.Instance.gameObject.GetComponent<ScientistController>().enabled = false;
            ScientistController.Instance.Animator.SetFloat("velocity", 0f);
            Destroy(this.gameObject);
        }
    }
}
