﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearAfterClickPanel : ATutorialPanel
{
    public override void OnBeginPanel()
    {
    }

    public override void OnEndPanel()
    {
    }

    public override void OnNextButtonClick()
    {
        this.gameObject.SetActive(false);
    }

}
