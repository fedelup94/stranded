﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstWavePanel : ATutorialPanel
{
    [SerializeField] ToggleController toggleController;
    public override void OnBeginPanel()
    {
        toggleController.Toggle();
    }

    public override void OnEndPanel()
    {
    }

    public override void OnNextButtonClick()
    {
        this.gameObject.SetActive(false);
    }
}
