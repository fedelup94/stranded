﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialManager : MonoBehaviour
{
    private static TutorialManager instance;
    private int currentTutorialPanel;
    [SerializeField] private List<ATutorialPanel> tutorialPanels;

    public static TutorialManager Instance { get => instance;}
    public UnityEvent nextPanelEvent;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            nextPanelEvent = new UnityEvent();
            nextPanelEvent.AddListener(OnNextPanelEvent);
            currentTutorialPanel = -1;
        }
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        for (int i = 0; i < tutorialPanels.Count; i++)
            tutorialPanels[i].gameObject.SetActive(false);
        nextPanelEvent.Invoke();
    }


    public void OnNextPanelEvent()
    {
        if(currentTutorialPanel >= 0)
        {
            tutorialPanels[currentTutorialPanel].EndPanel();
            tutorialPanels[currentTutorialPanel].gameObject.SetActive(false);
        }
        currentTutorialPanel++;
        if (currentTutorialPanel >= tutorialPanels.Count)
            return;
        tutorialPanels[currentTutorialPanel].gameObject.SetActive(true);
        tutorialPanels[currentTutorialPanel].BeginPanel();
    }
}
