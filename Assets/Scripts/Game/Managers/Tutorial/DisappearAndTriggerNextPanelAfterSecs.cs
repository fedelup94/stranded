﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearAndTriggerNextPanelAfterSecs : ATutorialPanel
{
    [SerializeField] private float seconds;
    [SerializeField] private GameObject triggerPrefab;
    private AfterTimePanelTrigger panelTrigger;
    public override void OnBeginPanel()
    {
        panelTrigger = Instantiate(triggerPrefab).GetComponent<AfterTimePanelTrigger>();
    }
    public override void OnEndPanel()
    {
    }

    public override void OnNextButtonClick()
    {
        panelTrigger.TriggerNextPanel(seconds);
        this.gameObject.SetActive(false);
    }
}
