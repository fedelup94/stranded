﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrigNextPanelAfterPlacement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        EventsManager.Manager.hasPlaceObject.AddListener(OnObjectPlaced);
    }

    public void OnObjectPlaced(APlaceable placeable)
    {
        TutorialManager.Instance.nextPanelEvent.Invoke();
        Destroy(this.gameObject);
    }
}
