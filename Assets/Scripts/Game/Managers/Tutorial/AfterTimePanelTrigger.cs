﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterTimePanelTrigger : MonoBehaviour
{
    IEnumerator NextPanelAfterSecsCoroutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        TutorialManager.Instance.nextPanelEvent.Invoke();
        Destroy(this.gameObject);
    }

    public void TriggerNextPanel(float seconds)
    {
        StartCoroutine(NextPanelAfterSecsCoroutine(seconds));
    }
}
