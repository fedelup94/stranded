﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPoints : MonoBehaviour
{

    [SerializeField]
    private Transform[] wayPoints;
    [SerializeField]
    private float stopDistance;

    private Animator animator;
    private int currentWaypoint;
    private float test;


    private NavMeshAgent agent;

    public NavMeshAgent Agent { get => agent; set => agent = value; }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
        currentWaypoint = Random.Range(0, wayPoints.Length);
        Agent.SetDestination(wayPoints[currentWaypoint].position);

    }

    // Update is called once per frame
    void Update()
    {
        if (HasArrived())
        {
            //currentWaypoint = (currentWaypoint + 1) % wayPoints.Length;
            currentWaypoint = Random.Range(0, wayPoints.Length);
            Agent.SetDestination(wayPoints[currentWaypoint].position);
        }
        else
        {
            Agent.isStopped = false;
        }
    }

    private bool HasArrived()
    {
        if (Agent.pathPending)
            return false;

        return Agent.remainingDistance <= stopDistance;
    }
}
