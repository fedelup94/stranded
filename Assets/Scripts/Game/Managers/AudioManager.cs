﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour
{


    [Header("Music")]
    [SerializeField]
    private AudioSource musicSource;

    [SerializeField]
    private AudioClip safeMusic;

    [SerializeField]
    private AudioClip fightMusic;



    [Header("Weapon")]

    [SerializeField]
    private Transform ammoRifleSound;
    private WeaponSoundManager ammoRifleWS;

    [SerializeField]
    private Transform laserRifleSound;
    private WeaponSoundManager laserRifleWS;
    private bool laserOn;

    [SerializeField]
    private Transform rocketLauncherSound;
    private WeaponSoundManager rocketLauncherWS;

    [SerializeField]
    private AudioSource weaponPickup;



    [Header("Tower")]

    [SerializeField]
    private AudioSource towerPlace;

    [SerializeField]
    private AudioSource towerPickup;

    [SerializeField]
    private AudioSource towerRecharging;

    [SerializeField]
    private AudioSource towerRifleShoot;

    [SerializeField]
    private AudioSource towerLaserShoot;
    private bool towerLaserOn;



    [Header("Wave")]
    [SerializeField]
    private AudioSource waveStarted;

    [SerializeField]
    private AudioSource waveEnded;



    [Header("Misc")]
    [SerializeField]
    private AudioSource pickup;

    [SerializeField]
    private AudioSource buttonSound;


    private static AudioManager instance;
    private AudioSource playingMusic;

    public static AudioManager Instance { get => instance; }



    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
            playingMusic = musicSource;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ammoRifleWS = ammoRifleSound.GetComponent<WeaponSoundManager>();
        laserRifleWS = laserRifleSound.GetComponent<WeaponSoundManager>();
        rocketLauncherWS = rocketLauncherSound.GetComponent<WeaponSoundManager>();
        PlaySafeMusicBegin();
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Music
    public void PauseMusic()
    {
        playingMusic.Pause();
    }

    public void UnPauseMusic()
    {
        playingMusic.UnPause();
    }

    public void PlaySafeMusic()
    {
        playingMusic.Stop();

        PlayWaveEnded();
        playingMusic.clip = safeMusic;
        playingMusic.PlayDelayed(waveEnded.clip.length/2);
    }

    public void PlayFightMusic()
    {
        playingMusic.Stop();

        PlayWaveStarted();
        playingMusic.clip = fightMusic;
        playingMusic.PlayDelayed(waveStarted.clip.length);
        //playingMusic.Play();
    }

    private void PlaySafeMusicBegin()
    {
        playingMusic.Stop();
        playingMusic.clip = safeMusic;
        playingMusic.Play();
    }
    #endregion

    #region Wave
    public void PlayWaveStarted()
    {
        waveStarted.Play();
    }

    public void PlayWaveEnded()
    {
        waveEnded.Play();
    }
    #endregion

    #region Misc
    public void PlayPickupSound()
    {
        pickup.Play();
    }

    public void PlayButtonSound()
    {
        buttonSound.Play();
    }
    #endregion

    #region Weapon
    public void PlayAmmoRifleSound()
    {
        //ammoRifleWS/*ammoRifleSound*/.PlayWeaponShoot();
        //ammoRifleShoot.Play();
        StartCoroutine(PlayRifleSoundRoutine());
    }

    private IEnumerator PlayRifleSoundRoutine()
    {
        ammoRifleWS/*ammoRifleSound*/.PlayWeaponShoot();
        yield return null;
    }

    public void PlayAmmoRifleReload()
    {
        ammoRifleWS/*ammoRifleSound*/.PlayWeaponReload();
        //ammoRifleReload.Play();
    }

    public void PlayLaserRifleSound()
    {
        if (!laserOn)
        {
            laserRifleWS.PlayWeaponShoot();
            laserOn = true;
        }
    }

    public void StopLaserRifleSound()
    {
        if (laserOn)
        {
            laserRifleWS.PauseWeaponShoot();
            laserOn = false;
        }
    }

    public void PlayLaserRifleReload()
    {
        laserRifleWS.PlayWeaponReload();
    }

    public void PlayRocketLauncherSound()
    {
        rocketLauncherWS.PlayWeaponShoot();
    }

    public void PlayRocketLauncherReload()
    {
        rocketLauncherWS.PlayWeaponReload();
    }

    public void PlayWeaponPickupSound()
    {
        weaponPickup.Play();
    }
    #endregion

    #region Tower
    public void PlayTowerPlaceSound()
    {
        towerPlace.Play();
    }

    public void PlayTowerPickupSound()
    {
        towerPickup.Play();
    }

    public void PlayTowerRifleSound()
    {
        towerRifleShoot.Play();
    }

    public void PlayTowerLaserSound()
    {
        if (!towerLaserOn)
        {
            towerLaserShoot.Play();
            towerLaserOn = true;
        }
    }

    public void StopTowerLaserSound()
    {
        if (towerLaserOn)
        {
            towerLaserShoot.Pause();
            towerLaserOn = false;
        }
    }



    #endregion


}
