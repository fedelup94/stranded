﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSoundManager : MonoBehaviour
{

    [SerializeField]
    private WeaponSoundPrefab weaponSound;

    private new AudioSource audio;

    public WeaponSoundPrefab WeaponSound { get => weaponSound; set => weaponSound = value; }


    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayWeaponShoot()
    {
        if (WeaponSound.ShootSound != null)
        {
                audio.clip = WeaponSound.ShootSound;
                audio.pitch = WeaponSound.ShootPitch;
                audio.volume = WeaponSound.ShootVolume;
                audio.Play();
        }
    }

    public void PauseWeaponShoot()
    {
        audio.Pause();
    }

    public void PlayWeaponReload()
    {
        if (WeaponSound.ReloadSound != null)
        {
            audio.clip = WeaponSound.ReloadSound;
            audio.pitch = WeaponSound.ReloadPitch;
            audio.volume = WeaponSound.ReloadVolume;
            audio.Play();
        }
    }
}
