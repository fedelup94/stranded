﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Weapon Container", menuName = "CustomSO/Music/Weapon Container")]
public class WeaponSoundPrefab : ScriptableObject
{

    [Header("Shoot")]
    [SerializeField]
    private AudioClip shootSound;
    [SerializeField]
    [Range(0f,1f)]
    private float shootVolume = 1;
    [SerializeField]
    [Range(-3f,3f)]
    private float shootPitch = 1;

    [Header("Reload")]
    [SerializeField]
    private AudioClip reloadSound;
    [SerializeField]
    [Range(0f, 1f)]
    private float reloadVolume = 1;
    [SerializeField]
    [Range(-3f, 3f)]
    private float reloadPitch = 1;

    public float ReloadPitch { get => reloadPitch; set => reloadPitch = value; }
    public float ReloadVolume { get => reloadVolume; set => reloadVolume = value; }
    public AudioClip ReloadSound { get => reloadSound; set => reloadSound = value; }
    public float ShootPitch { get => shootPitch; set => shootPitch = value; }
    public float ShootVolume { get => shootVolume; set => shootVolume = value; }
    public AudioClip ShootSound { get => shootSound; set => shootSound = value; }
}
