﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour, IDamageable
{

    private static Ship instance;

    public static Ship Instance { get => instance; }



    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool HasDied()
    {
        return ShipEnergyManager.Manager.Amount(EnergyType.Health) <= 0;
    }

    public void takeDamage(float damage)
    {
        if (ShipEnergyManager.Manager.Amount(EnergyType.Laser) <= 0)
        {
            ShipEnergyManager.Manager.LoseEnergy(EnergyType.Health, damage);
        }
        //else if (ShipEnergyManager.Manager.Amount(EnergyType.Health) > 0)
        //{
        //    ShipEnergyManager.Manager.LoseEnergy(EnergyType.Health, damage);
        //}

        if (ShipEnergyManager.Manager.Amount(EnergyType.Health) <= 0) { /*Debug.Log("LA NAVE E' MORTA");*/ EventsManager.Manager.gameOver.Invoke(); }

    }
}
