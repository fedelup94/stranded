﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyShield : MonoBehaviour, IDamageable
{
    [SerializeField] private Color maxHealthColor;
    [SerializeField] private Color minHealthColor;
    [SerializeField] private float maxHealth;

    private float currentHealth;
    private Renderer renderer;

    private void Awake()
    {
        currentHealth = maxHealth;
        renderer = GetComponent<Renderer>();
        renderer.sharedMaterial.color = maxHealthColor;
        renderer.sharedMaterial.SetColor("_EmissionColor", maxHealthColor * 0.5f);
    }

    public void takeDamage(float damage)
    {
        currentHealth -= damage;
        Color lerpColor = Color.Lerp(maxHealthColor, minHealthColor, (maxHealth - currentHealth) / maxHealth);
        renderer.sharedMaterial.color = lerpColor;
        renderer.sharedMaterial.SetColor("_EmissionColor", lerpColor * 0.5f);
        if (currentHealth <= 0)
            this.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool HasDied()
    {
        return currentHealth <= 0f;
    }
}
