﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class HealthChanged : UnityEvent<float> { }

public class Health : MonoBehaviour
{
    [SerializeField] private float maxHealth;

    private float currentHealth;

    public void ChangeHealth(float amountToAdd)
    {
        currentHealth += amountToAdd;
        if (currentHealth >= maxHealth)
            currentHealth = maxHealth;
    }
}
