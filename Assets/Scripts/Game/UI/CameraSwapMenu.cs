﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum QualitySettings { Low = 1, Medium = 2, High = 3, Ultra = 5 };


public class CameraSwapMenu : MonoBehaviour
{
    [Header("PreviewPositions")]
    [SerializeField]
    private Transform[] cameraPositions = null;

    [Header("Panels")]
    [SerializeField]
    private UnityEngine.UI.Text levelText = null;
    [SerializeField]
    private Transform howToPlayPanel = null;
    [SerializeField]
    private Transform creditsPanel = null;
    [SerializeField]
    private Transform settingsPanel = null;
    [SerializeField]
    private Lean.Localization.LeanLocalizedText qualityLevelText = null;

    [Header("VersionText")]
    [SerializeField]
    private TMPro.TextMeshProUGUI versionText;

    QualitySettings qualitySettings;

    private Lean.Localization.LeanLocalizedText levelTextLocalization;
    private int actualMapSelected;

    // Start is called before the first frame update
    void Start()
    {
        actualMapSelected = 0;
        for (int i = 0; i < cameraPositions.Length; i++)
        {
            cameraPositions[i].parent.gameObject.SetActive(false);
        }
        cameraPositions[actualMapSelected].parent.gameObject.SetActive(true);
        transform.position = cameraPositions[actualMapSelected].position;
        levelTextLocalization = levelText.transform.parent.GetComponentInChildren<Lean.Localization.LeanLocalizedText>();
        UpdateText();
        CalculateQualitySettings();

        versionText.text = "v " + Application.version;

    }



    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowNextLevel()
    {
        if (actualMapSelected < cameraPositions.Length - 1)
        {
            cameraPositions[actualMapSelected].parent.gameObject.SetActive(false);
            actualMapSelected++;
            cameraPositions[actualMapSelected].parent.gameObject.SetActive(true);
            transform.position = cameraPositions[actualMapSelected].position;
            Debug.Log("Livello: " + actualMapSelected);
            UpdateText();
        }
        

    }

    public void ShowPrevLevel()
    {
        if (actualMapSelected > 0)
        {
            cameraPositions[actualMapSelected].parent.gameObject.SetActive(false);
            actualMapSelected--;
            cameraPositions[actualMapSelected].parent.gameObject.SetActive(true);
            transform.position = cameraPositions[actualMapSelected].position;
            Debug.Log("Livello: " + actualMapSelected);
            UpdateText();
        }

    }

    public void QuitGame()
    {
        Application.Unload();
    }

    public void NewGame()
    {
        SceneLoader.Instance.LoadScene(actualMapSelected + 1);
    }

    public void ToggleHowToPlayPanel()
    {
        if (howToPlayPanel.gameObject.activeInHierarchy)
        {
            howToPlayPanel.gameObject.SetActive(false);
        }
        else
            DisableAllPanelsExcept(howToPlayPanel);
    }

    public void ToggleCreditsPanel()
    {
        if (creditsPanel.gameObject.activeInHierarchy)
        {
            creditsPanel.gameObject.SetActive(false);
        }
        else
            DisableAllPanelsExcept(creditsPanel);
    }

    public void ToggleSettingsPanel()
    {
        if (settingsPanel.gameObject.activeInHierarchy)
        {
            settingsPanel.gameObject.SetActive(false);
        }
        else
            DisableAllPanelsExcept(settingsPanel);
    }

    private void DisableAllPanelsExcept(Transform GOToTurnOn)
    {
        if (howToPlayPanel.gameObject.activeInHierarchy)
            howToPlayPanel.gameObject.SetActive(false);
        if (creditsPanel.gameObject.activeInHierarchy)
            creditsPanel.gameObject.SetActive(false);
        if (settingsPanel.gameObject.activeInHierarchy)
            settingsPanel.gameObject.SetActive(false);
        GOToTurnOn.gameObject.SetActive(true);

    }

    private void CalculateQualitySettings()
    {
        switch (UnityEngine.QualitySettings.GetQualityLevel())
        {
            case 1:
                SetQualityAndText(QualitySettings.Low);
                break;
            case 2:
                SetQualityAndText(QualitySettings.Medium);
                break;
            case 3:
                SetQualityAndText(QualitySettings.High);
                break;
            case 5:
                SetQualityAndText(QualitySettings.Ultra);
                break;
        }
    }

    private void SetQualityAndText(QualitySettings quality)
    {
        switch (quality)
        {
            case QualitySettings.Low:
                qualitySettings = QualitySettings.Low;
                qualityLevelText.TranslationName = "QUALITYLOW";
                break;
            case QualitySettings.Medium:
                qualitySettings = QualitySettings.Medium;
                qualityLevelText.TranslationName = "QUALITYMEDIUM";
                break;
            case QualitySettings.High:
                qualitySettings = QualitySettings.High;
                qualityLevelText.TranslationName = "QUALITYHIGH";
                break;
            case QualitySettings.Ultra:
                qualitySettings = QualitySettings.Ultra;
                qualityLevelText.TranslationName = "QUALITYULTRA";
                break;
        }
    }

    public void ChangeQualitySettings()
    {
        switch (qualitySettings)
        {
            case QualitySettings.Low:
                UnityEngine.QualitySettings.SetQualityLevel((int)QualitySettings.Medium, true);
                SetQualityAndText(QualitySettings.Medium);
                break;
            case QualitySettings.Medium:
                UnityEngine.QualitySettings.SetQualityLevel((int)QualitySettings.High, true);
                SetQualityAndText(QualitySettings.High);
                break;
            case QualitySettings.High:
                UnityEngine.QualitySettings.SetQualityLevel((int)QualitySettings.Ultra, true);
                SetQualityAndText(QualitySettings.Ultra);
                break;
            case QualitySettings.Ultra:
                UnityEngine.QualitySettings.SetQualityLevel((int)QualitySettings.Low, true);
                SetQualityAndText(QualitySettings.Low);            
                break;
        }
        Debug.Log(UnityEngine.QualitySettings.GetQualityLevel());
    }

    public void ChangeLanguage()
    {
        Settings.Instance.NextLanguage();
    }

    public void UpdateText()
    {
        levelTextLocalization.gameObject.GetComponent<Lean.Localization.LeanLocalizedText>().enabled = true;
        if (actualMapSelected == 0)
        {
            levelTextLocalization.TranslationName = "MENUTUTORIALDESCRIPTION";
            levelTextLocalization.gameObject.GetComponent<UnityEngine.UI.Text>().text = " " + levelTextLocalization.gameObject.GetComponent<UnityEngine.UI.Text>().text;
            levelTextLocalization.gameObject.GetComponent<Lean.Localization.LeanLocalizedText>().enabled = false;
            levelText.text = "";
        }
        else
        {
            levelTextLocalization.TranslationName = "MENULEVELDESCRIPTION";
            levelText.text = actualMapSelected.ToString("00");
        }

    }
}
