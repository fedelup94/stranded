﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
[System.Serializable]
public class OnToggleEvent : UnityEvent<bool> { };
public class ToggleController : MonoBehaviour
{
    [SerializeField] Image toggleImage;
    [SerializeField] bool isOn;
    [SerializeField] Vector2 onAnchorValue;
    [SerializeField] Vector2 offAnchorValue;
    [SerializeField] OnToggleEvent OnToggle;
    [SerializeField] UnityEvent OnToggleON;
    [SerializeField] UnityEvent OnToggleOFF;
    // Start is called before the first frame update

    private void OnEnable()
    {
        positionToggle();
    }

    private void positionToggle()
    {
        if (isOn)
        {
            toggleImage.rectTransform.anchorMax = new Vector2(onAnchorValue.y, toggleImage.rectTransform.anchorMax.y);
            toggleImage.rectTransform.anchorMin = new Vector2(onAnchorValue.x, toggleImage.rectTransform.anchorMin.y);
            OnToggleON.Invoke();
        }
        else
        {
            toggleImage.rectTransform.anchorMax = new Vector2(offAnchorValue.y, toggleImage.rectTransform.anchorMax.y);
            toggleImage.rectTransform.anchorMin = new Vector2(offAnchorValue.x, toggleImage.rectTransform.anchorMin.y);
            OnToggleOFF.Invoke();
        }
        OnToggle.Invoke(isOn);
    }

   
    public void Toggle()
    {
        isOn = !isOn;
        positionToggle();
    }

    public void DisableToggleUI()
    {
        if (isOn)
        {
            Toggle();
        }
        gameObject.SetActive(false);
    }

    public void EnableToggleUI()
    {
        gameObject.SetActive(true);
    }
}
