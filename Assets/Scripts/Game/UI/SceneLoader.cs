﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private static SceneLoader instance;
    [SerializeField] private Animator loadingPanelAnimator;
    [SerializeField] private Canvas loadingCanvas;
    private float loadDelay = 1f;

    public static SceneLoader Instance { get => instance;}

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
            Destroy(this.gameObject);

    }

    public void LoadScene(int sceneIndex)
    {
        Debug.Log("Loading scene: " + sceneIndex);
        StartCoroutine(LoadSceneCoroutine(sceneIndex));
    }

    private IEnumerator LoadSceneCoroutine(int sceneIndex)
    {
        loadingCanvas.gameObject.SetActive(true);
        loadingPanelAnimator.SetTrigger("Show");
        yield return new WaitForSeconds(loadDelay);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        Application.backgroundLoadingPriority = UnityEngine.ThreadPriority.Low;
        operation.completed += LoadingComplete;
    }

    public void LoadingComplete(AsyncOperation operation)
    {
        loadingPanelAnimator.SetTrigger("Hide");
        Debug.Log("LoadingComplete");
    }
}
