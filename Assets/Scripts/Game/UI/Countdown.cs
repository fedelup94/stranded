﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Countdown : MonoBehaviour
{

    private static Countdown instance;

    [SerializeField]
    private TMPro.TMP_Text text;

    private Animator animator;
    private int counter;

    public static Countdown Instance { get => instance;  }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
            animator = text.gameObject.GetComponent<Animator>();

        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    public void StartTimer()
    {
        StopAllCoroutines();
        text.gameObject.SetActive(true);
        counter = (int)WavesManager.Instance.CountDownTimer;
        animator.SetBool("CounterLessThan6", counter < 6);
        StartCoroutine(StartCountdown(counter));
    }

    public void RestartCountdown()
    {
        StopAllCoroutines();
        counter = (int)WavesManager.Instance.Timer;
        animator.SetBool("CounterLessThan6", counter < 6);
        StartCoroutine(StartCountdown(counter));
    }

    public void InterruptTimer()
    {
        StopAllCoroutines();
        text.gameObject.SetActive(false);
    }

    private IEnumerator StartCountdown(int timer)
    {
        counter = timer;
        while (counter > 0)
        {
            text.text = counter.ToString();
            yield return new WaitForSeconds(1.0f);
            counter--;
            animator.SetBool("CounterLessThan6", counter < 6);
        }
        text.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
