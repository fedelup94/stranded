﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OffscreenShipIndicator : MonoBehaviour
{
    RectTransform indicatorTransform;
    [SerializeField] private TextMeshProUGUI distanceText;
    [SerializeField] private Image shipIcon;

    // Start is called before the first frame update
    private void Awake()
    {
        indicatorTransform = GetComponent<RectTransform>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 fromPosition = Camera.main.transform.position;
        Vector3 dir = (Ship.Instance.transform.position - fromPosition).normalized;

        Vector3 shipViewportPosition = Camera.main.WorldToViewportPoint(Ship.Instance.transform.position);
        //Debug.Log("SHIP POSITION: " + shipViewportPosition);
        bool isOffscreen = shipViewportPosition.x > 1 || shipViewportPosition.x < 0 || shipViewportPosition.y > 1 || shipViewportPosition.y < 0;
        if(!isOffscreen)
        {
            Hide();
            return;
        }

        Show();

        Vector2 anchoredPosition = Vector2.zero;
        anchoredPosition.x = Mathf.Clamp(shipViewportPosition.x, 0, 1);
        anchoredPosition.y = Mathf.Clamp(shipViewportPosition.y, 0, 1);

        //Debug.Log("IS OFFSCREEN: " + isOffscreen);
        indicatorTransform.anchoredPosition = anchoredPosition;
        indicatorTransform.anchorMin = anchoredPosition;
        indicatorTransform.anchorMax = anchoredPosition;
        indicatorTransform.pivot = anchoredPosition;

        float playerDistance = (Ship.Instance.transform.position - ScientistController.Instance.transform.position).magnitude;
        distanceText.text = ((int)playerDistance).ToString() + "mt";
    }

    private void Hide()
    {
        shipIcon.enabled = false;
        distanceText.enabled = false;
    }

    private void Show()
    {
        shipIcon.enabled = true;
        distanceText.enabled = true;
    }
}
