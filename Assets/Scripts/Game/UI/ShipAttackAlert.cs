﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAttackAlert : MonoBehaviour
{
    Animator shipAnimator;
    // Start is called before the first frame update
    private void Awake()
    {
        shipAnimator = GetComponent<Animator>();
    }
    void Start()
    {
        EventsManager.Manager.shipEnergyLeft.AddListener(OnShipEnergyChanged);
    }

    public void OnShipEnergyChanged(EnergyType energyType, float amount)
    {
        shipAnimator.SetTrigger("ShipUnderAttack");
    }
}
