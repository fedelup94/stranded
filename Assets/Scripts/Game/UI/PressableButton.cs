﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PressableButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private bool buttonPressed;
    [SerializeField] private bool buttonFirstPressed;
    [SerializeField] private bool buttonFirstReleased;

    public bool ButtonPressed { get => buttonPressed;}
    public bool ButtonFirstPressed { get => buttonFirstPressed;}
    public bool ButtonFirstReleased { get => buttonFirstReleased;}

    int pressedFrames = 1;
    int releasedFrames = 1;

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonPressed = true;
        buttonFirstPressed = true;
    }

    private void Update()
    {
        if (buttonFirstPressed && pressedFrames <= 0)
        {
            buttonFirstPressed = false;
            pressedFrames = 1;
        }
        else if (buttonFirstPressed && pressedFrames > 0)
            pressedFrames--;

        if (buttonFirstReleased && pressedFrames <= 0)
        {
            buttonFirstReleased = false;
            pressedFrames = 1;
        }
        else if (buttonFirstReleased && pressedFrames > 0)
            pressedFrames--;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonPressed = false;
        buttonFirstReleased = true;
    }

    private void OnDisable()
    {
        this.buttonPressed = false;
        this.buttonFirstPressed = false;
        this.buttonFirstReleased = false;
    }
}
