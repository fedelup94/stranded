﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootableItem : MonoBehaviour
{

    public static readonly float LOOT_PICKUP_RADIUS = 5;

    [SerializeField]
    private EnergyType energyType;
    [SerializeField]
    private float energyAmount;
    [SerializeField]
    private AudioSource pickupSFX;

    private Collider[] collidedObj;
    private bool firstTimePickup;

    public EnergyType EnergyType { get => energyType; set => energyType = value; }
    public float EnergyAmount { get => energyAmount; set => energyAmount = value; }
    public AudioSource PickupSFX { get => pickupSFX; set => pickupSFX = value; }


    // Start is called before the first frame update
    void Start()
    {
        firstTimePickup = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.layer == LayerMask.NameToLayer("Player") && !firstTimePickup)
        {
            Collect();
        }
    }

    private void Collect()
    {
        firstTimePickup = true;
        EnergyManager.Manager.ReceiveEnergy(EnergyType, EnergyAmount);
        AudioManager.Instance.PlayPickupSound();
        Destroy(gameObject);
    }



}
