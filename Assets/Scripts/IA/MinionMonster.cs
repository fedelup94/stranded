﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IA
{
    public class MinionMonster : Monster
    {

        [Header("Commander Settings")]
        [SerializeField]
        private Transform commander;
        [SerializeField]
        private float followCommanderStopDistance = 10f;

        private bool isCommanderAlive;


        public bool IsCommanderAlive { get => isCommanderAlive; set => isCommanderAlive = value; }
        public Transform Commander { get => commander; set => commander = value; }
        public float FollowCommanderStopDistance { get => followCommanderStopDistance; set => followCommanderStopDistance = value; }




        // Start is called before the first frame update
        void Start()
        {
            //Target = Ship.Instance.transform;
            SetState(new IdleStateMinion(this));
            IsCommanderAlive = (Commander != null);
        }

        // Update is called once per frame
        void Update()
        {
                CurrentState.Update();
        }
    }
}


