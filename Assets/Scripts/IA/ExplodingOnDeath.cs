﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingOnDeath : MonoBehaviour, IDamageable
{

    public enum ExplosionType { GAS, FIRE };

    [SerializeField]
    private float startingHp;
    [SerializeField]
    private float explosionRadius;
    [SerializeField]
    private ExplosionType explosionType;
    //[SerializeField]
    //private GameObject explosionEffect;
    [SerializeField]
    private GameObject explosionArea;

    public bool hasToDie = false;

    private float hp;

    public float Hp { get => hp; set => hp = value; }

    // Start is called before the first frame update
    void Start()
    {
        Hp = startingHp;
    }

    // Update is called once per frame
    void Update()
    {
        takeDamage(0);
    }

    public void takeDamage(float damage)
    {
        Hp -= (damage > 0) ? damage : 0;
        
        if (Hp <= 0 || hasToDie)
            Explode();
    }

    private void Explode()
    {
        //play dell'effetto
        //creo la pozza / mega fiammate
        var explosion = Instantiate(explosionArea, transform.position, new Quaternion());
        explosion.transform.localScale = explosion.transform.localScale * explosionRadius;
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }

    public bool HasDied()
    {
        throw new NotImplementedException();
    }
}


