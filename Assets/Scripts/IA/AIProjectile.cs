﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIProjectile : MonoBehaviour
{
    [SerializeField] float lifeTime;
    [SerializeField] float speed;
    [SerializeField] float damage;
    private Vector3 direction;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Deactivate", lifeTime);
    }

    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
    }

    private void Update()
    {
        transform.Translate(this.direction * speed * Time.deltaTime, Space.World);
        //Debug.DrawRay(this.transform.position, transform.forward * 10f, Color.blue);
    }

    private void OnTriggerEnter(Collider other)
    {
        IDamageable damageable = other.GetComponent<IDamageable>();
        if (damageable != null && other.gameObject.layer == LayerMask.NameToLayer("Player") && !damageable.HasDied())
        {
            damageable.takeDamage(damage);
            Deactivate();
        }

    }

    private void Deactivate()
    {
        this.gameObject.SetActive(false);
    }
}
