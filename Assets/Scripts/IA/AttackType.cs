﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace IA
{
    public abstract class AttackType : MonoBehaviour
    {

        [SerializeField]
        private float damage;
        [SerializeField]
        private float range;
        [SerializeField]
        private AudioSource attackSFX;

        public float Damage { get => damage; set => damage = value; }
        public float Range { get => range; set => range = value; }
        public AudioSource AttackSFX { get => attackSFX; set => attackSFX = value; }

        public abstract void Attack();

    }

}