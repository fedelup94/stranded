﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Wave", menuName = "CustomSO/Wave")]
public class WaveObject : ScriptableObject
{
    [SerializeField]
    private List<GameObject> enemiesToSpawn;
    [SerializeField]
    [Range(0, 500)]
    private float moneyReceived; 

    public List<GameObject> EnemiesToSpawn { get => enemiesToSpawn; set => enemiesToSpawn = value; }
    public float MoneyReceived { get => moneyReceived; set => moneyReceived = value; }
}
