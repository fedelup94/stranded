﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveTrail : MonoBehaviour
{

    [SerializeField]
    private Transform trailPrefab;

    [SerializeField]
    [Range(0,5)]
    private int numberOfTrailsToLeave;

    private Transform[] trailsGO;
    private int lastTrailUsed;
    

    // Start is called before the first frame update
    void Start()
    {
        if (trailPrefab != null)
        {
            trailsGO = new Transform[numberOfTrailsToLeave];
            lastTrailUsed = 0;
            for (int i = 0; i < numberOfTrailsToLeave; i++)
            {
                trailsGO[i] = Instantiate(trailPrefab.gameObject).transform;
                trailsGO[i].gameObject.SetActive(false);
            }
        }

    }

    public void LeaveFirstTrailAvailable()
    {
        if (trailPrefab != null)
        {
            if (Random.Range(0, 100) > 75)
            {
                trailsGO[lastTrailUsed].position = transform.position;
                trailsGO[lastTrailUsed].gameObject.SetActive(true);

                lastTrailUsed = (lastTrailUsed + 1) % numberOfTrailsToLeave;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        if (gameObject.activeInHierarchy)
            StartCoroutine(DestroyTrailsSlowly());
        else
        {
            if (trailsGO != null)
            {
                for (int i = 0; i < numberOfTrailsToLeave; i++)
                {
                    if (trailsGO[i] != null)
                        Destroy(trailsGO[i].gameObject, 1f);
                }
            }
        }

    }

    private IEnumerator DestroyTrailsSlowly()
    {
        if (trailsGO != null)
        {
            for (int i = 0; i < numberOfTrailsToLeave; i++)
            {
                if (trailsGO[i] != null)
                    Destroy(trailsGO[i].gameObject, 1f);
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null;

    }
}
