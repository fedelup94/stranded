﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour
{
    [SerializeField]
    private float damage;

    [SerializeField]
    [Range(0,10)]
    private float tickTimer;

    private Dictionary<IDamageable, float> objectsInside;

    // Start is called before the first frame update
    void Start()
    {
        objectsInside = new Dictionary<IDamageable, float>();

    }

    

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {


        if (!other.gameObject.name.Contains("Slime"))
        {
            IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
            if (damageable != null)
            {
                if (!objectsInside.ContainsKey(damageable))
                {
                    objectsInside[damageable] = float.NegativeInfinity;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
        if (damageable != null)
        {
            if (!objectsInside.TryGetValue(other.GetComponent<IDamageable>(), out float timer)) return;

            if (Time.time > timer)
            {
                objectsInside[damageable] = Time.time + tickTimer;
                damageable.takeDamage(damage);
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
        if (damageable != null)
            objectsInside.Remove(damageable);
    }
}
