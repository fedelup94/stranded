﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace IA
{

    public enum AnimationState { IDLE, RUN, ATTACK, DIE };

    public class Monster : MonoBehaviour, IDamageable
    {


        public static readonly float DespawnTime = 1.4f;
        private static Camera mainCamera;


        [Header("Stats")]
        [SerializeField]
        private float aggroRadius;
        [SerializeField]
        private float chasingStopDistance = 7.5f;
        [SerializeField]
        private float startingHp = 100f;
        [SerializeField]
        private Transform healthBar;
        [SerializeField]
        private Image hpBar;
        [SerializeField]
        private LootObject lootPrefab;
        [SerializeField]
        private Transform deathFX;
        [SerializeField]
        private AudioSource deathSFX;


        [Header("DEBUG")]
        public bool SHOWLIFE = false;

        //private static Transform player;
        //private static Transform ship;

        private NavMeshAgent agent;
        private IAState currentState;
        private Animator animator;
        private AnimationClip attackAnimationClip;
        private bool coroutineStarted;
        [SerializeField]
        private float hp;

        private AttackType attackType;
        private GameObject lootGO;
        private float healthShowTimer;
        private SplitAfterDeath splitAfterDeath;
        private Transform[] childs;
        private GameObject deathFXGO;

        public bool isDead;

        public NavMeshAgent Agent { get => agent; private set => agent = value; }
        public IAState CurrentState { get => currentState; set => currentState = value; }
        public float AggroRadius { get => aggroRadius; set => aggroRadius = value; }
        public float ChasingStopDistance { get => chasingStopDistance; set => chasingStopDistance = value; }
        public float Hp { get => hp; set => hp = value; }
        public Transform HealthBar { get => healthBar; set => healthBar = value; }
        public Image HpBar { get => hpBar; set => hpBar = value; }
        public AttackType AttackType { get => attackType; set => attackType = value; }
        public Animator Animator { get => animator; set => animator = value; }
        public AudioSource DeathSFX { get => deathSFX; set => deathSFX = value; }

        private void Awake()
        {
            coroutineStarted = false;
            Animator = GetComponent<Animator>();
            Agent = GetComponent<NavMeshAgent>();
            Hp = (startingHp <= 0) ? 100 : startingHp;
            isDead = false;
            AttackType = GetComponent<AttackType>();
            if (deathFX != null)
            {
                deathFXGO = Instantiate(deathFX.gameObject);
                deathFXGO.SetActive(false);
            }
        }


        // Start is called before the first frame update
        void Start()
        {
            mainCamera = Camera.main;
            if (lootPrefab != null)
            {
                lootGO = LootManager.GenerateLootGO(lootPrefab);
                lootGO.SetActive(false);
            }
            else
                lootGO = null;

            splitAfterDeath = GetComponent<SplitAfterDeath>();
            if (splitAfterDeath != null && splitAfterDeath.NumberOfChilds > 0)
            {
                childs = new Transform[splitAfterDeath.NumberOfChilds];
                for (int i = 0; i < splitAfterDeath.NumberOfChilds; i++)
                {
                    childs[i] = Instantiate(splitAfterDeath.ChildToSpawn, transform);
                    childs[i].gameObject.SetActive(false);
                    childs[i].localScale = splitAfterDeath.ChildToSpawn.localScale/2;
                }
            }
            SetState(new IdleState(this));
        }

        // Update is called once per frame
        void Update()
        {
            currentState.Update();
            if (healthShowTimer > 0)
            {
                HealthBar.gameObject.SetActive(true);
                HealthBar.LookAt(Camera.main.transform);
                healthShowTimer -= Time.deltaTime;
            } else
            {
                HealthBar.gameObject.SetActive(false);
            }
        }

        public void SetState(IAState state)
        {
            if (currentState != null)
                currentState.Exit();

            currentState = state;

            if (currentState != null)
            {
                currentState.Enter();
            }

        }

        internal bool IsPlayerNearby()
        {
            return (Vector3.Distance(ScientistController.Instance.transform.position, transform.position)) < AggroRadius;
        }

        internal bool IsShipNearby()
        {
            return (Vector3.Distance(Ship.Instance.transform.position, transform.position)) < AggroRadius;
        }

        internal void EnableAnimation(AnimationState animation)
        {
            switch (animation)
            {
                case AnimationState.IDLE:
                    break;
                case AnimationState.RUN:
                    Animator.SetBool("Run", true);
                    break;
                case AnimationState.ATTACK:
                    Animator.SetTrigger("Attack");
                    StartCoroutine(WaitForAttackClip(Animator.GetCurrentAnimatorClipInfo(0)[0].clip.length));
                    break;
                case AnimationState.DIE:
                    Animator.SetBool("Die", true);
                    break;
                default:
                    break;
            }
        }



        internal void DespawnAndCreateLoot()
        {
            if (deathFX != null)
            {
                deathFXGO.SetActive(true);
                deathFXGO.transform.position = transform.position;
                deathFXGO.GetComponent<ParticleSystem>().Play();
                GameObject.Destroy(deathFXGO, 2.5f);
            }
            if (lootGO != null)
            {
                lootGO.transform.position = transform.position;
                lootGO.SetActive(true);
            }
            if (splitAfterDeath != null && splitAfterDeath.NumberOfChilds > 0)
            {
                for (int i = 0; i < childs.Length; i++)
                {
                    childs[i].SetParent(transform.parent);
                    childs[i].transform.position = transform.position;
                    childs[i].transform.localPosition += new Vector3(UnityEngine.Random.Range(0f, 2f), 0, UnityEngine.Random.Range(0f, 2f));
                    childs[i].gameObject.SetActive(true);
                }
            }

            Destroy(gameObject);

        }

        private IEnumerator CheckWaveStatusAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            WavesManager.Instance.CheckActualWaveStatus();
            yield return null;
        }

        internal void DisableAnimation(AnimationState animation)
        {
            switch (animation)
            {
                case AnimationState.IDLE:
                    break;
                case AnimationState.RUN:
                    Animator.SetBool("Run", false);
                    break;
                case AnimationState.ATTACK:
                    Animator.ResetTrigger("Attack");
                    coroutineStarted = false;
                    break;
                case AnimationState.DIE:
                    break;
                default:
                    break;
            }
        }

        private IEnumerator WaitForAttackClip(float waitTime)
        {
            if (!coroutineStarted)
            {
                while (true)
                {
                    coroutineStarted = true;
                    yield return new WaitForSeconds(waitTime);
                    DisableAnimation(AnimationState.ATTACK);
                    yield break;
                }
            }
              
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.forward , AggroRadius);
        }

        public void takeDamage(float damage)
        {
            Hp -= (damage > 0) ? damage : 0;
            UpdateHealthBar();
            healthShowTimer = 3f;
            if (Hp <= 0)
                Die();
        }

        private void UpdateHealthBar()
        {
            HpBar.fillAmount = Hp / startingHp;
        }

        private void Die()
        {
            if (!isDead)
            {
                SetState(new DieState(this));
                //SpawnLoot();
                isDead = true;
            }
        }

        public bool HasDied()
        {
            return Hp <= 0;
        }
    }
}
