﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Loot", menuName = "CustomSO/Loot")]
public class LootObject : ScriptableObject
{

    [Header("Red Energy")]
    [Range(0.0f, 100.0f)]
    [SerializeField]
    private float redEnergyMin;

    [Range(0.0f, 100.0f)]
    [SerializeField]
    private float redEnergyMax;

    [Header("Green Energy")]
    [Range(0.0f, 100.0f)]
    [SerializeField]
    private float greenEnergyMin;

    [Range(0.0f, 100.0f)]
    [SerializeField]
    private float greenEnergyMax;

    [Header("Blue Energy")]
    [Range(0.0f, 100.0f)]
    [SerializeField]
    private float blueEnergyMin;

    [Range(0.0f, 100.0f)]
    [SerializeField]
    private float blueEnergyMax;

    [SerializeField]
    private List<WeaponSO> spawnableWeapons;

    public float RedEnergy { get => UnityEngine.Random.Range(redEnergyMin, redEnergyMax); }
    public float GreenEnergy { get => UnityEngine.Random.Range(greenEnergyMin, greenEnergyMax); }
    public float BlueEnergy { get => UnityEngine.Random.Range(blueEnergyMin, blueEnergyMax); }
    public List<WeaponSO> SpawnableWeapons { get => spawnableWeapons; set => spawnableWeapons = value; }

    

}
