﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IA
{

    public class MeleeAttackType : AttackType
    {
        [SerializeField]
        private LayerMask targetLayer;
        [SerializeField]
        private Transform meleeAttackPoint;

        private Vector3 attackForward;


        public LayerMask TargetLayer { get => targetLayer; set => targetLayer = value; }


        public override void Attack()
        {
            
            var hits = Physics.SphereCastAll(meleeAttackPoint.position, Range, meleeAttackPoint.forward);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider != null && hits[i].collider.gameObject.tag == "Player")
                {
                    var target = hits[i].collider.gameObject;
                    var targetDamage = target.GetComponent<IDamageable>();
                    AttackSFX.Play();
                    if (targetDamage != null)
                    {
                        targetDamage.takeDamage(Damage);
                        return;
                    }
                } else if (hits[i].collider != null && hits[i].collider.gameObject.tag == "Ship")
                {
                    var target = hits[i].collider.gameObject;
                    var targetDamage = target.GetComponent<IDamageable>();
                    AttackSFX.Play();
                    if (targetDamage != null)
                    {
                        targetDamage.takeDamage(Damage);
                        return;
                    }
                }
            }
            

        }

        private void Start()
        {

        }


        private void Update()
        {
            
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.black;
            Gizmos.DrawSphere(meleeAttackPoint.position, Range);
        }
    }

}