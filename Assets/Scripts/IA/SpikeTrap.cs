﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpikeTrap : MonoBehaviour
{

    [SerializeField]
    [Range(0,60)]
    private float resetTimer;
    [SerializeField]
    [Range(1, 50)]
    private float damage;

    private float timer;
    private Animator animator;

    public Animator Animator { get => animator; set => animator = value; }
    public float Timer { get => timer; set => timer = value; }
    public float Damage { get => damage; set => damage = value; }
    public float ResetTimer { get => resetTimer; set => resetTimer = value; }

    // Start is called before the first frame update
    void Start()
    {
        Timer = 0;
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Timer > 0)
            Timer -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Timer <= 0)
        {
            animator.SetTrigger("Attack");
            Timer = ResetTimer;
            var colliders = Physics.BoxCastAll(transform.position, gameObject.transform.localScale, Vector3.up);
            List<Transform> collidersGO = new List<Transform>();
            for (int i = 0; i < colliders.Length; i++)            
                collidersGO.Add(colliders[i].transform);

            collidersGO = collidersGO.Distinct().ToList();

            for (int i = 0; i < collidersGO.Count; i++)
            {
                    var targetToDamage = collidersGO[i].gameObject.GetComponent<IDamageable>();
                    if (targetToDamage != null)
                    {
                        targetToDamage.takeDamage(Damage);
                    }
            }
        }
    }


    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    Vector3 temp = gameObject.transform.localScale;
    //    Gizmos.DrawCube(transform.position, temp);
    //}

}
