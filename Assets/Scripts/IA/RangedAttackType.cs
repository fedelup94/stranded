﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IA
{


    public class RangedAttackType : AttackType
    {

        [SerializeField]
        private Transform rangedAttackSpawnPoint;

        private void Awake()
        {

        }

        private void Start()
        {

        }

        public override void Attack()
        {
            var bullet = BulletsPooler.SharedInstance.GetEnemyBullet();

            if (bullet != null)
            {
                bullet.transform.position = rangedAttackSpawnPoint.transform.position;
                bullet.transform.rotation = rangedAttackSpawnPoint.transform.rotation;
                bullet.SetDirection(rangedAttackSpawnPoint.transform.forward);
                bullet.gameObject.SetActive(true);
            } else 
            {
                var obj = Instantiate(BulletsPooler.SharedInstance.EnemyBulletToPool);
                bullet = obj.GetComponent<AIProjectile>();
                bullet.transform.position = rangedAttackSpawnPoint.transform.position;
                bullet.transform.rotation = rangedAttackSpawnPoint.transform.rotation;
                bullet.SetDirection(rangedAttackSpawnPoint.transform.forward);
                bullet.gameObject.SetActive(true);
                //Debug.Log("Proiettili per IA finiti");
            }
            AttackSFX.Play();


        }
    }
}
