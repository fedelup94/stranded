﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IA
{
    public abstract class IAState
    {



        protected Monster monster;
        protected AnimationState animation;

        protected IAState(Monster monster, AnimationState animation)
        {
            this.monster = monster;
            this.animation = animation;
        }

        public abstract void Update();

        public virtual void Enter() { monster.EnableAnimation(animation); }
        public virtual void Exit() { monster.DisableAnimation(animation); }



    }
}
