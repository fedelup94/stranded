﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace IA
{
    internal class MinionChasePlayerState : IAState
    {
        private MinionMonster m_monster;
        public static readonly float targetDistPrediction = 20;
        public static readonly float targetDistPredictionMult = 20;
        private Vector3 targetPosition;

        public MinionChasePlayerState(MinionMonster monster, AnimationState animation = AnimationState.RUN) : base(monster, animation)
        {
        }

        public override void Enter()
        {
            base.Enter();
            m_monster = (MinionMonster)monster;
            targetPosition = ScientistController.Instance.transform.position;
            monster.Agent.SetDestination(CalculateTarget());
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Update()
        {
            if (m_monster.IsCommanderAlive)
            {
                monster.Agent.SetDestination(CalculateTarget());
                if (IsTooFar())
                {
                    m_monster.Agent.isStopped = true;
                    m_monster.SetState(new FollowCommanderState((MinionMonster)monster));
                } else if (HasArrived())
                {
                    FaceTarget();
                    monster.Agent.isStopped = true;
                    monster.DisableAnimation(animation);
                    monster.EnableAnimation(AnimationState.ATTACK);
                } else
                {
                    monster.DisableAnimation(AnimationState.ATTACK);
                    monster.Agent.isStopped = false;
                }

            }
        }

        private Vector3 CalculateTarget()
        {
            var distance = (ScientistController.Instance.transform.position - monster.transform.position).magnitude;
            var speed = monster.Agent.velocity.magnitude;

            float prediction = 0;

            if (speed <= distance / targetDistPrediction)
                prediction = targetDistPrediction;
            else
                prediction = (distance / speed) * targetDistPredictionMult;

            var prevTargetPosition = targetPosition;
            targetPosition =ScientistController.Instance.transform.position;

            return targetPosition + (targetPosition - prevTargetPosition) * prediction;

        }

        private bool IsTooFar()
        {
            return (monster.Agent.remainingDistance > monster.AggroRadius);
        }

        private bool HasArrived()
        {

            if (monster.Agent.pathPending)
                return false;

            return monster.Agent.remainingDistance <= monster.ChasingStopDistance;

        }

        private void FaceTarget()
        {
            Vector3 direction = (ScientistController.Instance.transform.position - monster.transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            monster.transform.rotation = Quaternion.Slerp(monster.transform.rotation, lookRotation, Time.deltaTime * 5f);
        }

    }
}