﻿using UnityEngine;

namespace IA
{
    internal class IdleStateMinion : IAState
    {

        private MinionMonster m_monster;

        public IdleStateMinion(MinionMonster monster, AnimationState animation = AnimationState.IDLE) : base(monster, animation)
        {
        }

        public override void Enter()
        {
            base.Enter();
            m_monster = (MinionMonster)monster;

        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Update()
        {
            if (m_monster.IsCommanderAlive)
            {
                if (m_monster.IsPlayerNearby())
                    m_monster.SetState(new MinionChasePlayerState(m_monster));
                else if (m_monster.IsShipNearby())
                    m_monster.SetState(new MinionChaseShipState(m_monster));
                else
                    m_monster.SetState(new FollowCommanderState(m_monster));
            }
            else
                m_monster.SetState(new FleeState(m_monster));

        }
    }
}