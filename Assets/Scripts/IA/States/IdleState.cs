﻿namespace IA
{
    public class IdleState : IAState
    {



        public IdleState(Monster monster, AnimationState animation = AnimationState.IDLE) : base(monster, animation)
        {

        }

        public override void Enter()
        {
            base.Enter();

        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Update()
        {
            if (monster.IsPlayerNearby())
            {
                monster.SetState(new ChasePlayerState(monster));
            } else
            {
                monster.SetState(new ChaseShipState(monster));
            }
        }
    }
}