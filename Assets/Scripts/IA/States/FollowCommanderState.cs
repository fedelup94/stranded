﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace IA
{
    public class FollowCommanderState : IAState
    {

        private MinionMonster m_monster;
        private Transform commander;
        private NavMeshAgent commanderAgent;
        private static readonly float commanderBehindDistance = 2f;

        public FollowCommanderState(MinionMonster monster, AnimationState animation = AnimationState.RUN) : base(monster, animation)
        {

        }

        public override void Enter()
        {
            base.Enter();
            m_monster = (MinionMonster)monster;
            commander = m_monster.Commander.transform;
            commanderAgent = commander.GetComponent<NavMeshAgent>();
            var behindPosition = CommanderBehindPosition();
            m_monster.Agent.isStopped = false;
            m_monster.Agent.SetDestination(behindPosition);
            Debug.Log("FollowCommanderState");
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Update()
        {
            
            if (m_monster.IsCommanderAlive)
            {
                if (!m_monster.IsPlayerNearby())
                {
                    var behindPosition = CommanderBehindPosition();
                    m_monster.Agent.SetDestination(behindPosition);

                    if (HasArrived())
                    {
                        m_monster.Agent.isStopped = true;
                        monster.DisableAnimation(animation);
                    } else
                    {
                        
                        m_monster.Agent.isStopped = false;
                        monster.EnableAnimation(animation);
                    }
                } else
                {
                    //monster.Target = ScientistController.Instance.transform;
                    m_monster.SetState(new MinionChasePlayerState((MinionMonster)monster));
                }
            } else
            {
                m_monster.SetState(new FleeState((MinionMonster)monster));
            }
        }

        private Vector3 CommanderBehindPosition()
        {
            return commander.position + (-commanderAgent.velocity).normalized * commanderBehindDistance;
        }

        private bool HasArrived()
        {
            if (monster.Agent.pathPending)
                return false;
            //Debug.Log("RemainingDistance: " + monster.Agent.remainingDistance + "  StoppindDistance: " + Monster.FollowCommanderStopDistance);
            return monster.Agent.remainingDistance <= m_monster.FollowCommanderStopDistance;
        }
    }
}


