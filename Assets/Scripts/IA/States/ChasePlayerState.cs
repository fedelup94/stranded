﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace IA
{
    internal class ChasePlayerState : IAState
    {
        public static readonly float targetDistPrediction = 20;
        public static readonly float targetDistPredictionMult = 20;
        private Vector3 targetPosition;


        public ChasePlayerState(Monster monster, AnimationState animation = AnimationState.RUN) : base(monster, animation)
        {

        }

        public override void Enter()
        {
            base.Enter();
            targetPosition = ScientistController.Instance.transform.position;
            monster.Agent.SetDestination(CalculateTarget());
        }

        private Vector3 CalculateTarget()
        {
            var distance = (ScientistController.Instance.transform.position - monster.transform.position).magnitude;
            var speed = monster.Agent.velocity.magnitude;

            float prediction = 0;

            if (speed <= distance / targetDistPrediction)
                prediction = targetDistPrediction;
            else
                prediction = (distance / speed) * targetDistPredictionMult;

            var prevTargetPosition = targetPosition;
            targetPosition = ScientistController.Instance.transform.position;

            return targetPosition + (targetPosition - prevTargetPosition) * prediction;

        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Update()
        {
                monster.Agent.SetDestination(CalculateTarget());
            //if (IsTooFar())
            //{
            //    monster.Agent.isStopped = true;
            //    monster.SetState(new IdleState(monster));
            //}
            /*else*/ if (HasArrived())
            {
                FaceTarget();
                monster.Agent.isStopped = true;
                //monster.SetState(new AttackPlayerState(monster,this));
                monster.DisableAnimation(animation);
                monster.EnableAnimation(AnimationState.ATTACK);
            }
            else
            {
                //monster.DisableAnimation(AnimationState.ATTACK);
                monster.EnableAnimation(animation);
                monster.Agent.isStopped = false;
            }
            
            
        }



        private bool IsTooFar()
        {
            return (monster.Agent.remainingDistance > monster.AggroRadius);
        }

        private bool HasArrived()
        {
            if (monster.Agent.pathPending)
                return false;

            return monster.Agent.remainingDistance <= monster.ChasingStopDistance;
        }

        private void FaceTarget()
        {
            Vector3 direction = (ScientistController.Instance.transform.position - monster.transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            monster.transform.rotation = Quaternion.Slerp(monster.transform.rotation, lookRotation, Time.deltaTime * 7.5f);
        }
    }
}