﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.AI;

//namespace IA
//{
//    internal class MinionChaseState : IAState
//    {

//        private MinionMonster m_monster;

//        private static readonly float targetDistPrediction = 20;
//        private static readonly float targetDistPredictionMult = 20;
//        private Vector3 targetPosition;


//        public MinionChaseState(Monster monster, AnimationState animation = AnimationState.RUN) : base(monster, animation)
//        {
//        }

//        public override void Enter()
//        {
//            base.Enter();
//            m_monster = (MinionMonster)monster;

//            targetPosition = monster.Target.position;
//            monster.Agent.SetDestination(CalculateTarget());
//            //Debug.Log("MinionChaseState");

//        }

//        public override void Exit()
//        {
//            base.Exit();
//        }

//        public override void Update()
//        {
//            if (m_monster.IsCommanderAlive)
//            {
//                m_monster.Agent.SetDestination(CalculateTarget());
//                if (IsTooFar())
//                {
//                    m_monster.Agent.isStopped = true;
//                    m_monster.SetState(new FollowCommanderState((MinionMonster)monster));
//                }
//                else if (HasArrived())
//                {
//                    m_monster.Agent.isStopped = true;
//                    m_monster.DisableAnimation(animation);
//                    //Debug.Log("MinionChaseArrived");
//                    if (timeSinceLastAttack <= 0)
//                    {
//                        //ATTACK
//                        m_monster.EnableAnimation(AnimationState.ATTACK);
//                        timeSinceLastAttack = attackRate;                        
//                        Debug.Log("ATTACK");
//                    }
//                    else
//                    {
//                        //Debug.Log("TimeSinceLastAttack: " + timeSinceLastAttack);
//                        timeSinceLastAttack -= Time.deltaTime;
//                    }
//                    // hit the player
//                }
//                else
//                {
//                    m_monster.Agent.isStopped = false;
//                    //m_monster.Agent.SetDestination(CalculateTarget());
//                }
//            } else
//            {
//                m_monster.SetState(new FleeState((MinionMonster)monster));
//            }

//        }

//        private Vector3 CalculateTarget()
//        {
//            var distance = (monster.Target.transform.position - monster.transform.position).magnitude;
//            var speed = monster.Agent.velocity.magnitude;

//            float prediction = 0;

//            if (speed <= distance / targetDistPrediction)
//                prediction = targetDistPrediction;
//            else
//                prediction = (distance / speed) * targetDistPredictionMult;

//            var prevTargetPosition = targetPosition;
//            targetPosition = monster.Target.transform.position;

//            return targetPosition + (targetPosition - prevTargetPosition) * prediction;

//        }

//        private bool IsTooFar()
//        {
//            //Debug.Log("RemainingDistance: " + monster.Agent.remainingDistance + "  Aggro: " + monster.AggroRadius);
//            return (monster.Agent.remainingDistance > monster.AggroRadius);
//        }

//        private bool HasArrived()
//        {
//            if (monster.Agent.pathPending)
//                return false;
//            Debug.Log("RemainingDistance: " + monster.Agent.remainingDistance + "  StoppingDistance: " + monster.ChasingStopDistance);
//            return monster.Agent.remainingDistance <= monster.ChasingStopDistance;
//        }
//    }
//}