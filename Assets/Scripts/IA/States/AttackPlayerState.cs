﻿namespace IA
{
    internal class AttackPlayerState : IAState
    {

        private IAState previousState;

        public AttackPlayerState(Monster monster, IAState previousState, AnimationState animation = AnimationState.ATTACK) : base(monster, animation)
        {
            this.previousState = previousState;
        }

        public override void Enter()
        {
            base.Enter();
        }

        public override void Exit()
        {
            base.Exit();
            monster.SetState(previousState);

        }

        public override void Update()
        {

        }
    }
}