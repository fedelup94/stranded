﻿using UnityEngine;

namespace IA
{
    internal class DieState : IAState
    {

        private float despawnTimer;


        public DieState(Monster monster, AnimationState animation = AnimationState.DIE) : base(monster, animation)
        {
            despawnTimer = Monster.DespawnTime;
        }

        public override void Enter()
        {
            base.Enter();
            monster.Agent.isStopped = true;
            monster.EnableAnimation(animation);
            monster.DeathSFX.Play();
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Update()
        {
            if (despawnTimer <= 0)
                monster.DespawnAndCreateLoot();
            else               
                despawnTimer -= Time.deltaTime;            
        }
    }
}