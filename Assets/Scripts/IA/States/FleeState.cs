﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IA
{
    public class FleeState : IAState
    {
        private MinionMonster m_monster;
        private Vector3 destination;
        private float lookAheadDistance;

        public FleeState(MinionMonster monster, AnimationState animation = AnimationState.RUN) : base(monster, animation)
        {
        }

        public override void Enter()
        {
            base.Enter();
            m_monster = (MinionMonster)monster;
            destination = CalculatePath();
            lookAheadDistance = m_monster.AggroRadius;

        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Update()
        {
            if (IsFarEnough())
            {
                m_monster.Agent.isStopped = true;
                m_monster.SetState(new IdleState(m_monster));
            }
            else
            {
                m_monster.Agent.isStopped = false;
                m_monster.Agent.SetDestination(CalculatePath());
            }

        }

        private bool IsFarEnough()
        {
            return (Vector3.Distance(m_monster.transform.position, ScientistController.Instance.gameObject.transform.position) > m_monster.AggroRadius + (m_monster.AggroRadius * 1/4));
        }

        private Vector3 CalculatePath()
        {
            return m_monster.transform.position + (m_monster.transform.position - ScientistController.Instance.gameObject.transform.position).normalized * lookAheadDistance;
        }



    }
}


