﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


namespace IA
{
    public class SplitAfterDeath : MonoBehaviour
    {
        [SerializeField]
        private Transform childToSpawn;

        [Range(0,4)]
        [SerializeField]
        private int numberOfChilds;

        public Transform ChildToSpawn { get => childToSpawn; set => childToSpawn = value; }
        public int NumberOfChilds { get => numberOfChilds; set => numberOfChilds = value; }
    }
}