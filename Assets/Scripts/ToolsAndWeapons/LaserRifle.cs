﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRifle : APlayerWeapon, IPickable
{
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private ParticleSystem laserParticleSystem;
    [SerializeField] private LayerMask layerMask;

    private bool isFiring;
    private bool isOverheat;

    protected override void Awake()
    {
        base.Awake();
        isOverheat = false;
        isFiring = false;
        DeactivateLaser();
    }

    private void Update()
    {
        if (!lineRenderer.gameObject.activeInHierarchy)
        {
            if (magazineLeft < MagazineCapacity)
            {
                magazineLeft += Time.deltaTime * FireRate;
                if(magazineLeft >= magazineCapacity)
                {
                    magazineLeft = MagazineCapacity;
                    isOverheat = false;
                }
                EventsManager.Manager.magazineBulletsLeft.Invoke(this, magazineLeft);
            }
        }
        if (!isFiring || isOverheat)
            return;

        Vector3 direction = new Vector3(fire.transform.forward.x, 0.0f, fire.transform.forward.z);

        lineRenderer.SetPosition(0, fire.position);
        lineRenderer.SetPosition(1, fire.position + direction * range);
        

        float requestedEnergy = EnergyManager.Manager.LoseEnergy(EnergyType, Time.deltaTime * ammoWeight);

        if (requestedEnergy <= 0f)
        {
            DeactivateLaser();
            return;
        }

        ActivateLaser();

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(fire.position, direction, out hit, range, this.layerMask))
        {
            IDamageable dg = hit.collider.gameObject.GetComponent<IDamageable>();
            if (dg != null)
            {
                dg.takeDamage(damage * Time.deltaTime);
            }

        }

        magazineLeft -= Time.deltaTime * FireRate;
        capacityLeft -= Time.deltaTime * AmmoWeight;
        EventsManager.Manager.magazineBulletsLeft.Invoke(this, magazineLeft);
        EventsManager.Manager.capacityBulletsLeft.Invoke(this, capacityLeft);

        if (magazineLeft <= 0f)
        {
            magazineLeft = 0f;
            isOverheat = true;
            DeactivateLaser();
        }
    }

    private void ActivateLaser()
    {
        laserParticleSystem.Play();
        lineRenderer.gameObject.SetActive(true);
        if (AudioManager.Instance != null)
            AudioManager.Instance.PlayLaserRifleSound();

    }

    private void DeactivateLaser()
    {
        laserParticleSystem.Stop();
        lineRenderer.gameObject.SetActive(false);
        if (AudioManager.Instance != null)
            AudioManager.Instance.StopLaserRifleSound();

    }

    public override void Reload()
    { 

    }

    public override void Shoot(Transform target)
    {
        isFiring = true;
    }

    public override void OnStartAiming()
    {
        Shoot(null);
    }

    public override void OnStopAiming()
    {
        isFiring = false;
        DeactivateLaser();
    }
}
