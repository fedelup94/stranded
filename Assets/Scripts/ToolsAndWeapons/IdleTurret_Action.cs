﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IdleTurret_Action : Action
{
    [Tooltip("Minimum rotation (in degrees) of the tuttet's head around its y axis")]
    public SharedFloat minRotationAngle = 60;
    [Tooltip("Maximum rotation (in degrees) of the tuttet's head around its y axis")]
    public SharedFloat maxRotationAngle = 20;
    [Tooltip("The speed of te rotation around the turret's y axis")]
    public SharedFloat rotationSpeed = 100;
    [Tooltip("The minimum length of time that the turret should pause after each rotation")]
    public SharedFloat minPauseDuration = 0.5f;
    [Tooltip("The maximum length of time that the turret should pause after each rotation")]
    public SharedFloat maxPauseDuration = 1.25f;
    [Tooltip("The upper part of the turret which is able to rotate along its")]
    public SharedGameObject turretHead;

    public AWeapon turretWeapon;

    private SharedBool canStartRotate = true;
    private IEnumerator idleCoroutine;


    public override void OnStart()
    {
        base.OnStart();
        canStartRotate = true;
    }

    public override TaskStatus OnUpdate()
    {
        if (turretWeapon.IsEmpty())
        {
            return TaskStatus.Failure;
        }


        if (canStartRotate.Value)
        {
            float rotationAngle = UnityEngine.Random.Range(minRotationAngle.Value, maxRotationAngle.Value);
            idleCoroutine = RotateCoroutine(rotationAngle);
            StartCoroutine(idleCoroutine);
        }

        return TaskStatus.Running;
    }

    IEnumerator RotateCoroutine(float angle)
    {
        canStartRotate = false;
        float rotated = 0f;
        UnityEngine.Vector3 rotateAround = UnityEngine.Vector3.up;
        if (UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f)
            rotateAround = -UnityEngine.Vector3.up;
        while (rotated <= angle)
        {
            float currentAngle = rotationSpeed.Value * UnityEngine.Time.deltaTime;
            UnityEngine.Vector3 rotation = rotateAround * currentAngle;
            rotated += currentAngle;
            turretHead.Value.transform.Rotate(rotation);
            yield return null;
        }
        yield return new UnityEngine.WaitForSeconds(UnityEngine.Random.Range(minPauseDuration.Value, maxPauseDuration.Value));
        canStartRotate = true;
    }

    public override void OnEnd()
    {
        base.OnEnd();
        if(idleCoroutine != null)
        {
            StopCoroutine(idleCoroutine);
        }


    }

    public override void OnReset()
    {
        base.OnReset();
        if (idleCoroutine != null)
        {
            StopCoroutine(idleCoroutine);
        }
        canStartRotate = true;
    }
}
