﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LaserRifleUISlot : AWeaponUISlot
{
    [SerializeField] TextMeshProUGUI capacityText;
    [SerializeField] Slider overheatSlider;
    [SerializeField] TextMeshProUGUI weaponNameText;

    private bool isReloading;
    private Animator animator;
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public override void BindWeapon(APlayerWeapon weapon)
    {
        if (weapon == this.weapon)
            return;
        base.BindWeapon(weapon);
        overheatSlider.maxValue = 1f;
        weaponNameText.text = weapon.WeaponName;
        ChangeCapacity(weapon, weapon.CapacityLeft);
        ChangeMagazine(weapon, weapon.MagazineLeft);
        isReloading = false;

    }

    public override void ChangeCapacity(APlayerWeapon weapon, float newCapacity)
    {
        if(this.weapon == weapon)
        {
            float percentage = (int)((newCapacity / weapon.Capacity) * 100f);
            capacityText.text = percentage.ToString() + "%";
        }
    }

    public override void ChangeMagazine(APlayerWeapon weapon, float newMagazine)
    {
        if(this.weapon == weapon)
        {
            float overheat = (weapon.MagazineCapacity - newMagazine)/ weapon.MagazineCapacity;
            overheatSlider.value = overheat;
            if (overheat <= 0.01f)
                isReloading = false;
            else if (overheat >= 1f)
                isReloading = true;
             animator.SetBool("isReloading", isReloading);
        }
    }
}
