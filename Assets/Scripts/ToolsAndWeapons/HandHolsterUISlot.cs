﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandHolsterUISlot : MonoBehaviour
{
    [SerializeField] private List<AWeaponUISlot> aWeaponUISlots;
    [SerializeField] private AWeaponUISlot currentSlot;

    private void Start()
    {
        EventsManager.Manager.holsterWeaponChanged.AddListener(OnFrontWeaponChanged);
    }

    private void OnFrontWeaponChanged(HolsterType holsterType, APlayerWeapon weapon)
    {
        if(holsterType == HolsterType.Hand)
        {
            for (int i = 0; i < aWeaponUISlots.Count; i++)
            {
                aWeaponUISlots[i].gameObject.SetActive(false);
            }

            if (weapon != null)
            {
                for (int i = 0; i < aWeaponUISlots.Count; i++)
                {
                    if (aWeaponUISlots[i].WeaponType == weapon.EnergyType)
                    {
                        aWeaponUISlots[i].gameObject.SetActive(true);
                        aWeaponUISlots[i].BindWeapon(weapon);
                        currentSlot = aWeaponUISlots[i];
                        break;
                    }
                }
            }
        }
    }

    private void OnEnable()
    {
        if (EventsManager.Manager == null)
            return;
        EventsManager.Manager.capacityBulletsLeft.AddListener(OnChangeCapacity);
        EventsManager.Manager.magazineBulletsLeft.AddListener(OnChangeMagazine);
    }

    private void OnDisable()
    {
        if (EventsManager.Manager == null)
            return;
        EventsManager.Manager.capacityBulletsLeft.RemoveListener(OnChangeCapacity);
        EventsManager.Manager.magazineBulletsLeft.RemoveListener(OnChangeMagazine);
    }

    public void OnChangeCapacity(APlayerWeapon weapon, float newCapacity)
    {
        if (currentSlot == null)
            return;
        currentSlot.ChangeCapacity(weapon, newCapacity);
    }
    public void OnChangeMagazine(APlayerWeapon weapon, float newMagazine)
    {
        if (currentSlot == null)
            return;
        currentSlot.ChangeMagazine(weapon, newMagazine);
    }

    private void OnDestroy()
    {
        EventsManager.Manager.holsterWeaponChanged.RemoveListener(OnFrontWeaponChanged);
    }
}
