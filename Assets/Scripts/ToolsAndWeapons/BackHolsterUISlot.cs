﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BackHolsterUISlot : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI text;


    private void Awake()
    {
        image.enabled = false;
        text.enabled = false;
    }

    private void Start()
    {
        EventsManager.Manager.holsterWeaponChanged.AddListener(OnBackWeaponChanged);
    }

    private void OnBackWeaponChanged(HolsterType holsterType, APlayerWeapon weapon)
    {
        if(holsterType == HolsterType.Back)
        {
            if (weapon != null)
            {
                image.enabled = true;
                text.enabled = true;
                image.sprite = weapon.WeaponSprite;
                text.text = weapon.WeaponName;
            }
            else
            {
                image.enabled = false;
                text.enabled = false;
            }
                
        }
    }

    private void OnDestroy()
    {
        EventsManager.Manager.holsterWeaponChanged.RemoveListener(OnBackWeaponChanged);
    }
}
