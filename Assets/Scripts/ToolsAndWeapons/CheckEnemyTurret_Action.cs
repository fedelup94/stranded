﻿using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class CanSeeEnemy_Turret : Conditional
{
    [Tooltip("The Turret's field of view object")]
    public FieldOfView fieldOfView;
    [Tooltip("The object that is within sight")]
    public SharedTransform returnedObject;

    public override TaskStatus OnUpdate()
    {
        if (fieldOfView.target == null)
            return TaskStatus.Failure;
        returnedObject.Value = fieldOfView.target;
        //UnityEngine.Debug.Log("RETURNED OBJECT OK!!");
        return TaskStatus.Success;
    }
}
