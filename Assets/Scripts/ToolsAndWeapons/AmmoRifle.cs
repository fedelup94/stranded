﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoRifle : APlayerWeapon, IPickable
{
    [SerializeField] ParticleSystem muzzleFire;

    public override void Shoot(Transform target)
    {
        if (magazineLeft > 0)
        {
            Projectile bullet = BulletsPooler.SharedInstance.GetPooledBullet();
            AudioManager.Instance.PlayAmmoRifleSound();
            if (bullet != null)
            {
                muzzleFire.Play();
                bullet.transform.position = fire.position;
                bullet.transform.rotation = fire.rotation;
                Vector3 direction = fire.transform.forward;
                bullet.SetDirection(direction);
                //Debug.DrawRay(fire.position, fire.transform.forward * 10f, Color.red);
                bullet.gameObject.SetActive(true);
                //bullet.PlaySound();
            }
            magazineLeft--;
            EventsManager.Manager.magazineBulletsLeft.Invoke(this, magazineLeft);
        }
    }

    public override void Reload()
    {
        if (capacityLeft <= 0)
            return;
        if (magazineLeft >= magazineCapacity)
            return;
        else
        {
            float bulletsToFill = magazineCapacity - magazineLeft;
            Debug.Log("Bullets to fill: " + bulletsToFill);
            float energyToRefillMagazine = EnergyManager.Manager.LoseEnergy(EnergyType, bulletsToFill * ammoWeight);
            float bulletsRetrieved = energyToRefillMagazine / ammoWeight;
            magazineLeft += bulletsRetrieved;
            AudioManager.Instance.PlayAmmoRifleReload();
            EventsManager.Manager.magazineBulletsLeft.Invoke(this, magazineLeft);
        }
    }

    public override void OnStartAiming()
    {
    }

    public override void OnStopAiming()
    {
    }



}
