﻿using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class CheckEmptyTurret_Action : Action
{
    public AWeapon turretWeapon;
    public UnityEngine.Animator animator;

    public override TaskStatus OnUpdate()
    {
        if (turretWeapon.IsEmpty())
        {
            animator.SetBool("isEmpty", true);
            return TaskStatus.Failure;
        }
        else
        {
            animator.SetBool("isEmpty", false);
            return TaskStatus.Success;
        }

    }
}
