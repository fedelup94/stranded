﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    [SerializeField] private AcquisitionArea area;
    [SerializeField] private float damage;
    [SerializeField] private float timeToExplosion;
    [SerializeField] private Animator pulsingLightAnimator;
    [SerializeField] private ParticleSystem explosionEffect;
    [SerializeField] private LayerMask layerMask;

    private float countDownRate = 0.1f;

    private void Awake()
    {
        pulsingLightAnimator.SetFloat("velocity", 1f / timeToExplosion);
        explosionEffect.Stop();
    }
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (!(layerMask == (layerMask | (1 << other.gameObject.layer))))
            return;
        IDamageable damageable = other.GetComponent<IDamageable>();
        if (damageable == null)
            return;
        StartCoroutine(CountDownCoroutine());
    }

    IEnumerator CountDownCoroutine()
    {
        while(timeToExplosion >0)
        {
            timeToExplosion -= countDownRate;
            pulsingLightAnimator.SetFloat("velocity", 1f / timeToExplosion);
            yield return new WaitForSeconds(countDownRate);
        }
        Explode();
        Destroy(this.gameObject);


    }

    private void Update()
    {
    }

    // Update is called once per frame
    void Explode()
    {
  
        Collider[] colliders = Physics.OverlapSphere(transform.position, area.Radius, layerMask);
        for(int i = 0; i < colliders.Length; i++)
        {
            Collider col = colliders[i];
            IDamageable dam = col.GetComponent<IDamageable>();
            if(dam!=null)
            {
                dam.takeDamage(damage);
            }
        }

        // Unparent the particles from the shell.
        explosionEffect.transform.parent = null;
        // Play the particle system.
        explosionEffect.Play();
        // Once the particles have finished, destroy the gameobject they are on.
        Destroy(explosionEffect.gameObject, explosionEffect.main.duration);
        // Destroy the shell.
        Destroy(gameObject);
    }
}
