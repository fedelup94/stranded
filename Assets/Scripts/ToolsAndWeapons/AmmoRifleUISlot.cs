﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoRifleUISlot : AWeaponUISlot
{
    [SerializeField] Slider capacitySlider;
    [SerializeField] Slider magazineSlider;

    public override void BindWeapon(APlayerWeapon weapon)
    {
        base.BindWeapon(weapon);
        capacitySlider.maxValue = weapon.Capacity;
        magazineSlider.maxValue = weapon.MagazineCapacity;
        capacitySlider.minValue = 0f;
        magazineSlider.minValue = 0f;
    }
    public override void ChangeCapacity(APlayerWeapon weapon, float newCapacity)
    {
        if(this.weapon == weapon)
            capacitySlider.value = newCapacity;
    }

    public override void ChangeMagazine(APlayerWeapon weapon, float newMagazine)
    {
        if (this.weapon == weapon)
            magazineSlider.value = newMagazine;
    }
}
