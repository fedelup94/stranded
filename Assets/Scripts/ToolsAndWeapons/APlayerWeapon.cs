﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class APlayerWeapon : AWeapon, IPickable
{
    [SerializeField] protected Transform fire;

    public Transform Fire { get => fire; }

    public void Position(Transform transform)
    {
        this.transform.position = transform.position;
        this.transform.rotation = transform.rotation;
        this.transform.parent = transform;
    }

    protected void Start()
    {
        EventsManager.Manager.energyLeft.AddListener(OnEnergyChanged);
        //La capacity dell'arma del giocatore non è quella stabilita nello scriptable object relativo.
        capacity = EnergyManager.Manager.MaxAmount(EnergyType);
        capacityLeft = EnergyManager.Manager.Amount(EnergyType); 
    }

    public void OnEnergyChanged(EnergyType energyType, float energyAmount)
    {
        if (energyType == EnergyType)
        {
            capacityLeft = energyAmount;
            EventsManager.Manager.capacityBulletsLeft.Invoke(this, capacityLeft);
        }
    }

    public void OnPickUp(ScientistController scientist)
    {
        scientist.PickWeapon(this);
    }

}
