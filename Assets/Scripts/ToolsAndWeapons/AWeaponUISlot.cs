﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AWeaponUISlot : MonoBehaviour
{
    [SerializeField] protected APlayerWeapon weapon;
    [SerializeField] private EnergyType weaponType;

    public EnergyType WeaponType { get => weaponType;}

    public virtual void BindWeapon(APlayerWeapon weapon)
    {
        this.weapon = weapon;
    }

    private void OnEnable()
    {
        EventsManager.Manager.capacityBulletsLeft.AddListener(ChangeCapacity);
        EventsManager.Manager.magazineBulletsLeft.AddListener(ChangeMagazine);
    }

    private void OnDisable()
    {
        EventsManager.Manager.capacityBulletsLeft.RemoveListener(ChangeCapacity);
        EventsManager.Manager.magazineBulletsLeft.RemoveListener(ChangeMagazine);
    }

    public abstract void ChangeCapacity(APlayerWeapon weapon, float newCapacity);
    public abstract void ChangeMagazine(APlayerWeapon weapon, float newMagazine);

}
