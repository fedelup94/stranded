﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulsingColor : MonoBehaviour
{
    [SerializeField] private Color primaryColor;
    [SerializeField] private Color secondaryColor;
    [SerializeField] private float pulseTime;

    private Renderer renderer;
    private float currentPulseTime;

    // Start is called before the first frame update
    private void Awake()
    {
        renderer = GetComponent<Renderer>();
    }

    private void Update()
    {
        currentPulseTime += Time.deltaTime;
        if(currentPulseTime >= pulseTime)
        {
            Color temp = primaryColor;
            primaryColor = secondaryColor;
            secondaryColor = temp;
            currentPulseTime = 0;
        }

        Color currentColor = Color.Lerp(primaryColor, secondaryColor, (pulseTime - currentPulseTime) / pulseTime);
        renderer.material.color = currentColor;
        renderer.material.SetColor("_EmissionColor", currentColor);
    }

    public void SetColors(Color color)
    {
        primaryColor = color;
        secondaryColor = color;
    }
}
