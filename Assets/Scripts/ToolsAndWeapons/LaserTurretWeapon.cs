﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserTurretWeapon : AWeapon, IRefillable
{
    [SerializeField] ParticleSystem laserParticles;
    [SerializeField] private Slider ammoSlider;
    [SerializeField] private Transform fire;

    private AudioSource audio;
    private bool isLaserAudioOn;

    private LineRenderer lineRenderer;
    private bool isShooting = false;
    private Transform target;
    private IDamageable damageable;

    protected override void Awake()
    {
        base.Awake();
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
        isShooting = false;
        laserParticles.Stop();
        ammoSlider.value = ammoSlider.maxValue;
        capacityLeft = Capacity;
        audio = GetComponent<AudioSource>();
    }

    public override void Shoot(Transform target)
    {
        if(target != this.target)
        {
            this.target = target;
            this.damageable = target.gameObject.GetComponent<IDamageable>();
            OnStartAiming();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!isShooting || target == null || damageable.HasDied() || capacityLeft <= 0f)
            return;

        lineRenderer.SetPosition(0, fire.position);
        lineRenderer.SetPosition(1, target.position);
        capacityLeft -= Time.deltaTime * ammoWeight;
        this.damageable.takeDamage(this.damage * Time.deltaTime);
            
          
        if (capacityLeft <= 0)
        {
            OnStopAiming();
            capacityLeft = 0f;
        }

        if (damageable.HasDied())
            OnStopAiming();

        ammoSlider.value = capacityLeft * (ammoSlider.maxValue / Capacity);

    }

    public override void OnStartAiming()
    {
        isShooting = true;
        lineRenderer.enabled = true;
        lineRenderer.useWorldSpace = true;
        laserParticles.Play();
        PlayLaserSound();
    }

    private void PlayLaserSound()
    {
        if (!isLaserAudioOn)
        {
            audio.Play();
            isLaserAudioOn = true;
        }
    }

    private void StopLaserSound()
    {
        if (isLaserAudioOn)
        {
            audio.Stop();
            isLaserAudioOn = false;
        }
    }

    public override void OnStopAiming()
    {
        laserParticles.Stop();
        isShooting = false;
        lineRenderer.useWorldSpace = false;
        lineRenderer.enabled = false;
        StopLaserSound();
    }

    public override void Reload()
    {
    }

    public bool Refill(float energyAmount)
    {
        if (capacityLeft >= Capacity)
            return true;
        capacityLeft += energyAmount;
       
        if (capacityLeft >= Capacity)
        {
            capacityLeft = Capacity;
            ammoSlider.value = ammoSlider.maxValue;
            return true;
        }
        else
        {
            ammoSlider.value = (capacityLeft * ammoSlider.maxValue) / Capacity;
            return false;
        }
    }

    public bool IsFull()
    {
        return capacityLeft >= Capacity;
    }
}
