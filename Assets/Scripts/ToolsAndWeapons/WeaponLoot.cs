﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WeaponLoot : MonoBehaviour
{
    [SerializeField] private GameObject weaponPrefab;
    [SerializeField] private APlayerWeapon weapon;
    private APlayerWeapon playerWeapon;
    private ScientistController scientist;
    private Animator animator;

    [SerializeField] private Image newWeapon;
    [SerializeField] private TextMeshProUGUI newWeaponName;

    [SerializeField] private Image oldWeapon;
    [SerializeField] private TextMeshProUGUI oldWeaponName;

    [SerializeField] private Image buttonImage;
    [SerializeField] private Sprite getWeaponSprite;
    [SerializeField] private Sprite switchWeaponSprite;
    // Start is called before the first frame update

    private void Awake()
    {
        GameObject weaponObject = Instantiate(weaponPrefab, transform.position, transform.rotation);
        weaponObject.transform.parent = this.transform;
        weapon = weaponObject.GetComponent<APlayerWeapon>();
        animator = GetComponent<Animator>();
    }

    private void SetWeaponsInfo()
    {
        if(weapon != null)
        {
            newWeapon.enabled = true;
            newWeapon.sprite = weapon.WeaponSprite;
            newWeaponName.text = weapon.WeaponName;
        }
        else
        {
            newWeapon.enabled = false;
            newWeaponName.text = "";

        }
        if (playerWeapon != null)
        {
            buttonImage.sprite = switchWeaponSprite;
            oldWeapon.enabled = true;
            oldWeapon.sprite = playerWeapon.WeaponSprite;
            oldWeaponName.text = playerWeapon.WeaponName;
        }
        else
        {
            oldWeapon.enabled = false;
            oldWeaponName.text = "";
            buttonImage.sprite = getWeaponSprite;

        }
    }



    public void Switch()
    {
        APlayerWeapon tempWeapon = weapon;
        weapon = playerWeapon;
        if (weapon != null)
        {
            weapon.Position(transform);
        }
            
        playerWeapon = weapon;
        scientist.PickWeapon(tempWeapon);
        SetWeaponsInfo();

        if (weapon == null)
            Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animator.SetBool("isPlayerNear", true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            scientist = other.gameObject.GetComponent<ScientistController>();
            if (scientist == null)
                return;
            playerWeapon = scientist.HoldingWeapon;

            SetWeaponsInfo();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            scientist = null;
            playerWeapon = null;
            SetWeaponsInfo();
            animator.SetBool("isPlayerNear", false);
        }
         

    }
}
