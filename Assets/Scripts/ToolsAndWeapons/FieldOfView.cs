﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FieldOfView : MonoBehaviour
{
    [SerializeField] private string layerName;
    private Dictionary<Transform, IDamageable> targets;
    
    public Transform target
    {
        get
        {
            if (targets.Count <= 0)
                return null;
            return targets.First().Key;
        }
    }

    private void Awake()
    {
        targets = new Dictionary<Transform, IDamageable>();
    }

    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log("COLLISION!");
        if (other.gameObject.layer == LayerMask.NameToLayer(layerName) && other.gameObject.activeInHierarchy)
        {
            IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
            if (damageable != null && !damageable.HasDied())
                targets[other.gameObject.transform] = damageable;
        }

        targets = targets.OrderBy(x => Vector3.Distance(this.transform.position, x.Key.position)).ToDictionary(keySelector: z => z.Key, elementSelector: y => y.Value);
    }

    private void FixedUpdate()
    {
        if (targets.Count <= 0)
            return;
        targets = targets.Where(p => !p.Value.HasDied() && p.Key.gameObject.activeInHierarchy).ToDictionary(p=> p.Key, p=>p.Value);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(layerName))
        {
            IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
            if (damageable != null)
                targets.Remove(other.gameObject.transform);
        }
    }
}
