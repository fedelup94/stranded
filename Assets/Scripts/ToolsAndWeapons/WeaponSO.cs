﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeaponSO", menuName = "CustomSO/Objects/WeaponSO")]
public class WeaponSO : ScriptableObject
{
    [Header("Common to Tool and Player weapons")]
    [SerializeField] private string weaponName;
    [SerializeField] private EnergyType energyType;
    [SerializeField] private float ammoWeight;
    [SerializeField] private float fireRate;
    [SerializeField] private Sprite weaponSprite;
    [SerializeField] private float damage;
    [SerializeField] private float range;

    [Header("Specific to Tool weapons")]
    [SerializeField] private float capacity;


    [Header("Specific to Player weapons")]
    [SerializeField] private float magazineCapacity;
    [SerializeField] private string animLayer;

    public string WeaponName { get => weaponName;}
    public float Capacity { get => capacity;}
    public float MagazineCapacity { get => magazineCapacity;}
    public string AnimLayer { get => animLayer;}
    public float FireRate { get => fireRate;}
    public EnergyType EnergyType { get => energyType; }
    public Sprite WeaponSprite { get => weaponSprite;}
    public float Damage { get => damage;}
    public float Range { get => range; }
    public float AmmoWeight { get => ammoWeight;}
}
