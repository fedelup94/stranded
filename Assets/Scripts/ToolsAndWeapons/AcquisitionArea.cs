﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AcquisitionArea : MonoBehaviour
{
    [SerializeField] private float radius;

    public float Radius { get => radius;}

    private void Start()
    {
        transform.localScale = new Vector3(radius, transform.localScale.y, radius);
    }

    public void RadiusScale(float scale)
    {
        transform.localScale = new Vector3(radius * scale, transform.localScale.y, radius * scale);
    }
}
