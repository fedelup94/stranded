﻿using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class AttackEnemyTurret_Action : Action
{
    [Tooltip("The object that is within sight")]
    public SharedTransform turretTarget;
    [Tooltip("The object from which projectiles are fired")]
    public UnityEngine.Animator animator;
    [Tooltip("The speed of te rotation around the turret's y axis")]
    public SharedFloat rotationSpeed = 100;
    [Tooltip("The upper part of the turret which is able to rotate along its")]
    public SharedTransform turretHead;
    [Tooltip("The turret's weapon")]
    public AWeapon turretWeapon;
    [Tooltip("The turret's weapon fire rate")]
    public SharedFloat firerate;

    private bool canShoot;
    private IEnumerator shootCoroutine;
    private IDamageable damageable;

    public override void OnStart()
    {
        base.OnStart();
        animator.SetBool("isAiming", true);
        canShoot = true;
        turretWeapon.OnStartAiming();
        IDamageable damageable = turretTarget.Value.GetComponent<IDamageable>();
    }

    public override TaskStatus OnUpdate()
    {
        if(turretTarget.Value != null)
        {
            UnityEngine.Vector3 directionToTarget = turretTarget.Value.position - transform.position;
            UnityEngine.Quaternion targetRotation = UnityEngine.Quaternion.LookRotation(directionToTarget);
            UnityEngine.Vector3 rotation = UnityEngine.Quaternion.Lerp(turretHead.Value.rotation, targetRotation, UnityEngine.Time.deltaTime * rotationSpeed.Value).eulerAngles;
            turretHead.Value.rotation = UnityEngine.Quaternion.Euler(0f, rotation.y, 0f);
            UnityEngine.Debug.DrawRay(turretHead.Value.position, turretHead.Value.forward * 10f, UnityEngine.Color.red);

            if (canShoot)
            {
                if (turretWeapon.IsEmpty())
                {
                    animator.SetBool("isEmpty", true);
                    return TaskStatus.Failure;
                }
                shootCoroutine = ShootCoroutine();
                StartCoroutine(shootCoroutine);
            }
        }

        return TaskStatus.Running;
    }

    IEnumerator ShootCoroutine()
    {
        canShoot = false;
        turretWeapon.Shoot(turretTarget.Value);
        yield return new UnityEngine.WaitForSeconds(firerate.Value);
        canShoot = true;

    }

    public override void OnEnd()
    {
        base.OnEnd();
        animator.SetBool("isAiming", false);
        StopCoroutine(shootCoroutine);
        turretWeapon.OnStopAiming();
    }
}
