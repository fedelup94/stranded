﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncher : APlayerWeapon
{
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private LayerMask layerMask;
    private bool isFiring;
    Transform enemyTarget;
    IDamageable enemyTargetDamageable;
    IEnumerator lockTargetCoroutine;
    IEnumerator laserBlinkCoroutine;

    protected override void Awake()
    {
        base.Awake();
        isFiring = false;
        DeactivateLaser();
    }

    private void Update()
    {
        if (!isFiring)
            return;

        Vector3 direction = new Vector3(fire.transform.forward.x, 0.0f, fire.transform.forward.z);

        lineRenderer.SetPosition(0, fire.position);
        lineRenderer.SetPosition(1, fire.position + direction * range);

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(fire.position, direction, out hit, range, layerMask))
        {
            IDamageable dg = hit.collider.gameObject.GetComponent<IDamageable>();
            if (dg != null)
            {
                if (enemyTargetDamageable == dg)
                    return;
                else
                {
                    Debug.Log("Target lost!, different dg");
                    if (lockTargetCoroutine != null)
                        StopCoroutine(lockTargetCoroutine);
                    enemyTarget = hit.collider.gameObject.transform;
                    enemyTargetDamageable = dg;
                    lockTargetCoroutine = LockTargetCoroutine();
                    StartCoroutine(lockTargetCoroutine);
                }
           
            }
            else
            {
                //Debug.Log("Target lost!, no dg");
                TargetLost();
            }

        }
        else
        {
            //Debug.Log("Target lost!, no hit");
            TargetLost();
           
        }
    }

    private void TargetLost()
    {
        enemyTargetDamageable = null;
        if (lockTargetCoroutine != null)
            StopCoroutine(lockTargetCoroutine);
        if (laserBlinkCoroutine != null)
            StopCoroutine(laserBlinkCoroutine);
        lineRenderer.startColor = Color.red;

    }

    private IEnumerator LaserBlinkCoroutine()
    {
        float blinkTime = fireRate - 1f;
        float blinkFrame = 0.15f;
        float timePassed = 0f;
        Color laserColor = Color.green;
        while (timePassed < blinkTime)
        {
            lineRenderer.startColor = laserColor;
            yield return new WaitForSeconds(blinkFrame);
            laserColor = laserColor == Color.green ? Color.red : Color.green;
            timePassed += blinkFrame;
        }
        lineRenderer.startColor = Color.green;
        yield return new WaitForSeconds(1f);
    }

    private IEnumerator LockTargetCoroutine()
    {
        float lockTimePassed = 0f;
        laserBlinkCoroutine = LaserBlinkCoroutine();
        StartCoroutine(laserBlinkCoroutine);
        while (lockTimePassed < this.fireRate)
        {
            lockTimePassed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        StopCoroutine(laserBlinkCoroutine);
        lineRenderer.startColor = Color.green;
        Shoot(enemyTarget);
        TargetLost();
        DeactivateLaser();
    }

    private void ActivateLaser()
    {
        lineRenderer.gameObject.SetActive(true);
        isFiring = true;
        lineRenderer.startColor = Color.red;
    }

    private void DeactivateLaser()
    {
        lineRenderer.gameObject.SetActive(false);
        isFiring = false;
    }

    public override void Reload()
    {
        if (capacityLeft <= 0)
            return;
        if (magazineLeft >= magazineCapacity)
            return;
        else
        {
            float bulletsToFill = magazineCapacity - magazineLeft;
            AudioManager.Instance.PlayRocketLauncherReload();
            Debug.Log("Bullets to fill: " + bulletsToFill);
            float energyToRefillMagazine = EnergyManager.Manager.LoseEnergy(EnergyType, bulletsToFill * ammoWeight);
            float bulletsRetrieved = energyToRefillMagazine / ammoWeight;
            magazineLeft += bulletsRetrieved;
            EventsManager.Manager.magazineBulletsLeft.Invoke(this, magazineLeft);
        }
        ActivateLaser();
    }

    public override void Shoot(Transform target)
    {
        if (magazineLeft > 0)
        {
            ResearchProjectile bullet = BulletsPooler.SharedInstance.GetPooledResearchBullet();
            if (bullet != null)
            {
                AudioManager.Instance.PlayRocketLauncherSound();
                bullet.transform.position = fire.position;
                bullet.transform.rotation = fire.rotation;
                Vector3 direction = fire.transform.forward;
                bullet.SetTarget(target);
                bullet.SetDamage(this.damage);
                //Debug.DrawRay(fire.position, fire.transform.forward * 10f, Color.red);
                bullet.gameObject.SetActive(true);
            }
            magazineLeft--;
            EventsManager.Manager.magazineBulletsLeft.Invoke(this, magazineLeft);
        }

    }

    public override void OnStartAiming()
    {
        if(magazineLeft > 0f)
            ActivateLaser();
    }

    public override void OnStopAiming()
    {
        isFiring = false;
        if (lockTargetCoroutine != null)
            StopCoroutine(lockTargetCoroutine);
        if (laserBlinkCoroutine != null)
        StopCoroutine(laserBlinkCoroutine);
        DeactivateLaser();
    }
}
