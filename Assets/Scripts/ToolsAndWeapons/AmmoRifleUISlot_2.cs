﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AmmoRifleUISlot_2 : AWeaponUISlot
{
    [SerializeField] TextMeshProUGUI capacityText;
    [SerializeField] TextMeshProUGUI magazineText;
    [SerializeField] TextMeshProUGUI weaponNameText;
    public override void BindWeapon(APlayerWeapon weapon)
    {
        base.BindWeapon(weapon);
        weaponNameText.text = weapon.WeaponName;
        ChangeCapacity(weapon, weapon.CapacityLeft);
        ChangeMagazine(weapon, weapon.MagazineLeft);
    }

    public override void ChangeCapacity(APlayerWeapon weapon, float newCapacity)
    {

        if(this.weapon == weapon)
        {
            Debug.Log(newCapacity);
            Debug.Log(weapon.AmmoWeight);
            float capacityToBullets = newCapacity / weapon.AmmoWeight;
            Debug.Log("Capacity bullets: " + capacityToBullets);
            capacityText.text = "/" + ((int)capacityToBullets).ToString();
        }
          
    }

    public override void ChangeMagazine(APlayerWeapon weapon, float newMagazine)
    {
        if(this.weapon == weapon)
            magazineText.text = ((int)newMagazine).ToString();

    }
}
