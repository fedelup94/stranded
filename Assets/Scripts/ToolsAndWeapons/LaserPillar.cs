﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserPillar : MonoBehaviour, IRefillable
{
    [SerializeField] private Transform top;
    [SerializeField] private Transform bottom;
    [SerializeField] private Slider energySlider;
    [SerializeField] private List<EnergyWall> energyWalls;

    [SerializeField] private float maxEnergy;
    private float currentEnergy;
    private List<LaserPillar> nearPillars;

    public Transform Top { get => top;}
    public Transform Bottom { get => bottom;}

    private void Awake()
    {
        for (int i = 0; i < energyWalls.Count; i++)
            energyWalls[i].gameObject.SetActive(false);
        currentEnergy = maxEnergy;
        energySlider.maxValue = maxEnergy;
        energySlider.value = currentEnergy;
        nearPillars = new List<LaserPillar>();
    }

    public bool Refill(float energyAmount)
    {
        if (energyAmount <= 0f)
            return false;
        currentEnergy += energyAmount;
        energySlider.value = currentEnergy;
        Reconnect();
        if (currentEnergy >= maxEnergy)
        {
            currentEnergy = maxEnergy;
            return true;
        }
        else
            return false;
    }

    public bool IsFull()
    {
        return currentEnergy >= maxEnergy;
    }

    public bool IsEmpty()
    {
        return currentEnergy <= 0f;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LaserPillar"))
        {
            //Debug.Log("My position: " + transform.position + ",Other pillar's: " + other.transform.position);
            if(other.gameObject.transform.parent != null)
            {
                LaserPillar otherPillar = other.transform.parent.gameObject.GetComponent<LaserPillar>();

                if (otherPillar == null)
                    return;

                if (nearPillars.Count < energyWalls.Count && !nearPillars.Contains(otherPillar))
                    nearPillars.Add(otherPillar);

                if (currentEnergy <= 0f)
                    return;

                OnConnect(otherPillar);
            }
          
        }
    }

    private void Reconnect()
    {
        int count = nearPillars.Count;
        for (int i = 0; i < count; i++)
        {
            if (nearPillars[i] != null && nearPillars[i].gameObject.activeInHierarchy)
                OnConnect(nearPillars[i]);
            else
            {
                nearPillars.Remove(nearPillars[i]);
                count--;
            }
        }
    }

    public void OnConnect(LaserPillar otherPillar)
    {
        for (int i = 0; i < energyWalls.Count; i++)
        {
            if (energyWalls[i].gameObject.activeInHierarchy && energyWalls[i].OtherPillar == otherPillar)
            {
                return;
            }
        }

        for (int i = 0; i < energyWalls.Count; i++)
        {
            if (!energyWalls[i].gameObject.activeInHierarchy)
            {
                energyWalls[i].gameObject.SetActive(true);
                energyWalls[i].OnConnect(otherPillar);
                break;
            }
        }
    }

    public void OnDisconnect(LaserPillar otherPillar)
    {
        for (int i = 0; i < energyWalls.Count; i++)
        {
            if (energyWalls[i] == null)
                continue;
            if (energyWalls[i].gameObject.activeInHierarchy && energyWalls[i].OtherPillar == otherPillar)
            {
                energyWalls[i].gameObject.SetActive(false);

            }
        }
    }

    public float TakeEnergy(float energy)
    {
        if (currentEnergy > 0)
        {
            currentEnergy -= energy;
            energySlider.value = currentEnergy;
            return energy;
        }

        for (int i = 0; i < energyWalls.Count; i++)
        {
            if (energyWalls[i].gameObject.activeInHierarchy)
            {
                energyWalls[i].gameObject.SetActive(false);
            }
        }

        energySlider.value = 0f;
        return 0f;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("LaserPillar"))
        {
            if (other.gameObject.transform.parent != null)
            {

                LaserPillar otherPillar = other.transform.parent.gameObject.GetComponent<LaserPillar>();

                if (otherPillar == null)
                    return;

                if (nearPillars.Contains(otherPillar))
                    nearPillars.Remove(otherPillar);

                OnDisconnect(otherPillar);
            }

        }

    }

    private void OnDisable()
    {
        for(int i = 0; i < nearPillars.Count; i++)
        {
            nearPillars[i].OnDisconnect(this);
        }
    }
}
