﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RifleTurretWeapon : AWeapon, IRefillable
{
    [SerializeField] private Slider ammoSlider;
    [SerializeField] ParticleSystem muzzleEffect;
    [SerializeField] private Transform fire;

    private AudioSource audio;

    protected override void Awake()
    {
        base.Awake();
        ammoSlider.value = ammoSlider.maxValue;
        capacityLeft = Capacity;
        audio = GetComponent<AudioSource>();
    }

    public override void Reload()
    {
        
    }

    public override void Shoot(Transform target)
    {
        if(!IsEmpty())
        {
            Projectile bullet = BulletsPooler.SharedInstance.GetPooledBullet();
            if (bullet != null)
            {
                bullet.gameObject.SetActive(true);
                bullet.transform.position = fire.position;
                bullet.transform.rotation = fire.rotation;
                bullet.SetDirection(fire.transform.forward);
            }
            muzzleEffect.Play();
            audio.PlayOneShot(audio.clip);
            capacityLeft -= 1;
            ammoSlider.value = ((float)capacityLeft * (float)ammoSlider.maxValue) / (float)Capacity;
        }
    }

    public override void OnStartAiming()
    {
    }

    public override void OnStopAiming()
    {
    }

    public bool Refill(float energyAmount)
    {
        if (capacityLeft >= Capacity)
            return true;
        capacityLeft += energyAmount;

        if (capacityLeft >= Capacity)
        {
            capacityLeft = Capacity;
            ammoSlider.value = ammoSlider.maxValue;
            return true;
        }
        else
        {
            ammoSlider.value = (capacityLeft * ammoSlider.maxValue) / Capacity;
            return false;
        }
    }

    public bool IsFull()
    {
        return capacityLeft >= Capacity;
    }
}
