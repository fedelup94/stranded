﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchProjectile : MonoBehaviour
{
    [SerializeField] float lifeTime;
    [SerializeField] float speed;
    [SerializeField] float damage;
    [SerializeField] LayerMask layerMask;
    [SerializeField] private ParticleSystem explosionEffect;
    private Vector3 direction;
    private Rigidbody rb;
    private Transform target;
    private bool hasExploded;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        direction = Vector3.zero;
    }

    private void OnEnable()
    {
        explosionEffect.Stop();
        hasExploded = false;
    }
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Deactivate", lifeTime);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void SetDamage(float damage)
    {
        this.damage = damage;
    }

    private void Update()
    {
        if(target != null)
        {
            this.direction = (target.position - this.transform.position).normalized;
        }

        transform.Translate(this.direction * speed * Time.deltaTime, Space.World);
        //Debug.DrawRay(this.transform.position, transform.forward * 10f, Color.blue);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (hasExploded)
            return;
        Collider[] colliders = Physics.OverlapSphere(transform.position, 3.0f, layerMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            Collider col = colliders[i];
            IDamageable dam = col.GetComponent<IDamageable>();
            if (dam != null)
            {
                dam.takeDamage(damage);
            }
        }
        // Play the particle system.
        explosionEffect.Play();
        Invoke("Deactivate", explosionEffect.main.duration);
        hasExploded = true;

    }

    private void Deactivate()
    {
        explosionEffect.Stop();
        this.gameObject.SetActive(false);
    }
}
