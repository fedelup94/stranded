﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AWeapon : MonoBehaviour
{
    [SerializeField] private WeaponSO weaponInfo;

    protected string weaponName;
    protected float capacity;
    protected float magazineCapacity;
    protected string animLayer;
    protected float fireRate;
    protected EnergyType energyType;
    protected float damage;
    protected float range;
    protected float ammoWeight;
    protected float capacityLeft;
    protected float magazineLeft;
    protected Sprite weaponSprite;

    public string WeaponName { get => weaponName;}
    public float Capacity { get => capacity;}
    public float MagazineCapacity { get => magazineCapacity;}
    public string AnimLayer { get => animLayer;}
    public float FireRate { get => fireRate;}
    public EnergyType EnergyType { get => energyType;}
    public float Damage { get => damage;}
    public float Range { get => range;}
    public float AmmoWeight { get => ammoWeight;}
    public float CapacityLeft { get => capacityLeft;}
    public float MagazineLeft { get => magazineLeft; }
    public Sprite WeaponSprite { get => weaponSprite; }

    protected virtual void Awake()
    {
        this.weaponName = weaponInfo.WeaponName;
        this.capacity = weaponInfo.Capacity;
        this.magazineCapacity = weaponInfo.MagazineCapacity;
        this.animLayer = weaponInfo.AnimLayer;
        this.fireRate = weaponInfo.FireRate;
        this.energyType = weaponInfo.EnergyType;
        this.damage = weaponInfo.Damage;
        this.range = weaponInfo.Range;
        this.ammoWeight = weaponInfo.AmmoWeight;
        this.weaponSprite = weaponInfo.WeaponSprite;
        this.capacityLeft = capacity;
        this.magazineLeft = magazineCapacity;
    }

    public abstract void Shoot(Transform target);
    public abstract void OnStartAiming();
    public abstract void OnStopAiming();
    abstract public void Reload();

    public bool IsEmpty()
    {
        return capacityLeft <= 0;
    }
}
