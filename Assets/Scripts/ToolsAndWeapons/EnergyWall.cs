﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[System.Serializable]
public class EnergyWall : MonoBehaviour
{
    [SerializeField] private BoxCollider physicalCollider;
    [SerializeField] private BoxCollider triggerCollider;
    [SerializeField] private float dps;
    [SerializeField] LaserPillar parentPillar;
    [SerializeField] private MeshFilter backWall;
    [SerializeField] private MeshFilter frontWall;
    [SerializeField] private LayerMask layerMask;
    private NavMeshObstacle obstacle;

    int[] frontTris;
    int[] backTris;
    Mesh frontMesh;
    Mesh backMesh;
    MeshFilter meshFilter;

    LaserPillar otherPillar;
    Transform[] transforms;
    Vector3 direction;
    bool isConnected;
    Vector3 parentPrevPos;
    Vector3 otherPrevPos;

    public bool IsConnected { get => isConnected;}
    public LaserPillar OtherPillar { get => otherPillar;}
    public LaserPillar ParentPillar { get => parentPillar;}
    private List<IDamageable> damageables;

    private void Awake()
    {
        obstacle = GetComponent<NavMeshObstacle>();
        frontMesh = new Mesh();
        backMesh = new Mesh();

        frontWall.mesh = frontMesh;
        backWall.mesh = backMesh;

        frontTris = new int[6]
        {
            // lower left triangle
            0, 1, 2,
            // upper right triangle
            2, 1, 3
        };

        backTris = new int[6]
        {
            // lower left triangle
            0, 2, 1,
            // upper right triangle
            2, 3, 1
        };

        isConnected = false;
        damageables = new List<IDamageable>();
    }

    private void MeshUpdate()
    {
        //Debug.DrawRay(this.transform.position, direction * 1000, Color.red);
        if (otherPillar == null)
        {
            return;
        }

        if (parentPrevPos == parentPillar.transform.position && otherPrevPos == otherPillar.transform.position)
            return;

        //Debug.Log("RECALCULATE MESH!!!!");
        direction = (transforms[0].position - transforms[1].position).normalized;
        transform.forward = direction;

        frontMesh.Clear();
        backMesh.Clear();

        Vector3[] vertices = new Vector3[4]
        {
            transform.InverseTransformPoint(transforms[0].position),
            transform.InverseTransformPoint(transforms[1].position),
            transform.InverseTransformPoint(transforms[2].position),
            transform.InverseTransformPoint(transforms[3].position),
        };

        backMesh.vertices = vertices;
        backMesh.triangles = backTris;

        frontMesh.vertices = vertices;
        frontMesh.triangles = frontTris;

        physicalCollider.size = frontMesh.bounds.size;
        physicalCollider.center = frontMesh.bounds.center;

        triggerCollider.size = new Vector3(2f, frontMesh.bounds.size.y, frontMesh.bounds.size.z);
        triggerCollider.center = frontMesh.bounds.center;

        obstacle.size = new Vector3(0.05f, frontMesh.bounds.size.y, frontMesh.bounds.size.z);
        obstacle.center = frontMesh.bounds.center;

        parentPrevPos = parentPillar.transform.position;
        otherPrevPos = otherPillar.transform.position;

        
    }

    public void OnConnect(LaserPillar otherPillar)
    {
        if (otherPillar == null)
        {
            this.gameObject.SetActive(false);
            return;
        }

        this.otherPillar = otherPillar;
        this.transforms = new Transform[4] { parentPillar.Bottom, otherPillar.Bottom, parentPillar.Top, otherPillar.Top };
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!(layerMask == (layerMask | (1 << other.gameObject.layer))))
            return;
        IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
        if (damageable == null || damageable.HasDied() || damageables.Contains(damageable))
            return;
        damageables.Add(damageable);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!(layerMask == (layerMask | (1 << other.gameObject.layer))))
            return;
        IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
        if (damageable != null)
        {
            damageables.Remove(damageable);
        }
    }

    public void FixedUpdate()
    {
        int count = damageables.Count;
        for (int i = 0; i < count; i++)
        {
            if(damageables[i] == null || damageables[i].HasDied())
            {
                damageables.Remove(damageables[i]);
                count--;
            }
            else
                damageables[i].takeDamage(parentPillar.TakeEnergy(dps * Time.fixedDeltaTime));
        }

        MeshUpdate();
    }

    public void OnDisconnect()
    {
        this.transforms = null;
        this.otherPillar = null;
        damageables.Clear();
        frontMesh.Clear();
        backMesh.Clear();

        Vector3[] vertices = new Vector3[4]
        {
            Vector3.zero,
            Vector3.zero,
            Vector3.zero,
            Vector3.zero
        };
        frontMesh.vertices = vertices;
        backMesh.vertices = vertices;
        parentPrevPos = Vector3.zero;
        otherPrevPos = Vector3.zero;
    }

    private void OnDisable()
    {
        Debug.Log("Disabling wall");
        OnDisconnect();
    }
}
