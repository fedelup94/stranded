﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAudioButton : MonoBehaviour
{

    [SerializeField]
    Sprite audioOnSprite;
    [SerializeField]
    Sprite audioOffSprite;

    private UnityEngine.UI.Image image;
    private UnityEngine.UI.Text text;

    private Lean.Localization.LeanLocalizedText localizedTextComponent;

    // Start is called before the first frame update
    void Start()
    {
        localizedTextComponent = transform.parent.GetComponentInChildren<Lean.Localization.LeanLocalizedText>();
        image = GetComponent<UnityEngine.UI.Image>();
        text = transform.parent.GetComponentInChildren<UnityEngine.UI.Text>(true);

        image.sprite = (Settings.Instance.IsAudioOn) ? audioOnSprite : audioOffSprite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        if (Settings.Instance.IsAudioOn)
        {
            image.sprite = audioOffSprite;
            localizedTextComponent.TranslationName = "SETTINGSAUDIOOFF";
            //text.text = "Audio Off";
        }
        else
        {
            image.sprite = audioOnSprite;
            localizedTextComponent.TranslationName = "SETTINGSAUDIOON";
            //text.text = "Audio On";
        }

        Settings.Instance.ToggleAudio();
    }


}
