﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEnabledCheck : MonoBehaviour
{

    private static AudioEnabledCheck instance;

    public static AudioEnabledCheck Instance { get => instance; }

    private List<AudioSource> audioSources;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            instance = this;        
    }

    // Start is called before the first frame update
    void Start()
    {
        RefreshList();
        CheckSettingsOn();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void RefreshList()
    {
        audioSources = new List<AudioSource>();
        audioSources.AddRange(FindObjectsOfType<AudioSource>());
    }

    public void CheckSettingsOn()
    {
        if (Settings.Instance != null && Settings.Instance.IsAudioOn)
        {
            for (int i = 0; i < audioSources.Count; i++)
            {
                audioSources[i].enabled = true;
            }
        }
        else
        {
            for (int i = 0; i < audioSources.Count; i++)
            {
                audioSources[i].enabled = false;
            }
        }
    }
}
