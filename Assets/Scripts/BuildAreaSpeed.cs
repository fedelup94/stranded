﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildAreaSpeed : MonoBehaviour
{
    [SerializeField] private float slowSpeed;
    [SerializeField] private float normalSpeed;
    private float speed;
    private List<Magnet> magnets = new List<Magnet>();

    public float Speed { get => speed;}

    private void Awake()
    {
        speed = normalSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        Magnet magnet = other.gameObject.GetComponent<Magnet>();
        if (magnet == null)
            return;
        if(! magnets.Contains(magnet))
            magnets.Add(magnet);
        if (speed != slowSpeed)
            speed = slowSpeed;
        
    }

    private void OnTriggerExit(Collider other)
    {
        Magnet magnet = other.gameObject.GetComponent<Magnet>();
        if (magnet == null)
            return;
        magnets.Remove(magnet);
        if (magnets.Count == 0)
            speed = normalSpeed;

    }

    private void OnDisable()
    {
        speed = normalSpeed;
        magnets.Clear();
    }

}
