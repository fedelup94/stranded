﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringPlayerState : IPlayerState
{
    private ScientistController scientist;

    private Vector3 movementDirection;
    private float movementVelocity;
    private Vector3 aimingDirection;
    private bool needsToReload = false;
    private Transform enemyTarget;

    public void Enter(ScientistController scientist)
    {
        this.scientist = scientist;
        scientist.HandHolster.Weapon.OnStartAiming();
        scientist.StartCoroutine(ChangeAnimationLayerCoroutine("NoWeaponLayer", scientist.HandHolster.Weapon.AnimLayer, .2f));
        scientist.Animator.SetFloat("fireRate", scientist.HandHolster.Weapon.FireRate);
        EventsManager.Manager.reloadWeapon.AddListener(NeedsToReload);
        scientist.Animator.SetBool("isAiming", true);
    }

    IEnumerator ChangeAnimationLayerCoroutine(string LayerFrom, string LayerTo, float transitionTime)
    {
        float actualTime = transitionTime;
        while(actualTime >= 0)
        {
            actualTime -= Time.deltaTime;
            scientist.Animator.SetLayerWeight(scientist.Animator.GetLayerIndex(LayerFrom), actualTime/transitionTime);
            scientist.Animator.SetLayerWeight(scientist.Animator.GetLayerIndex(LayerTo), (transitionTime-actualTime)/transitionTime);
            yield return new WaitForEndOfFrame();
        }
        scientist.Animator.SetLayerWeight(scientist.Animator.GetLayerIndex(LayerFrom),0f);
        scientist.Animator.SetLayerWeight(scientist.Animator.GetLayerIndex(LayerTo), 1f);

    }
    public void Exit()
    {
        scientist.HandHolster.Weapon.OnStopAiming();
        scientist.StartCoroutine(ChangeAnimationLayerCoroutine(scientist.HandHolster.Weapon.AnimLayer, "NoWeaponLayer", .2f));
        EventsManager.Manager.reloadWeapon.RemoveListener(NeedsToReload);
        scientist.Animator.SetBool("forwardRun", false);
        scientist.Animator.SetBool("strafeLeftRun", false);
        scientist.Animator.SetBool("backwardRun", false);
        scientist.Animator.SetBool("strafeRightRun", false);
        scientist.Animator.SetBool("isAiming", false);
        enemyTarget = null;
    }

    public IPlayerState FixedUpdate()
    {
        Vector3 velocity = movementDirection * movementVelocity * scientist.MovementSpeed;
        scientist.Animator.SetFloat("velocity", movementVelocity);
        scientist.Controller.SimpleMove(velocity);

        return this;
    }

    public IPlayerState Update()
    {
        aimingDirection = (Vector3.forward * InputManager.Manager.AimingJoystick.Vertical + Vector3.right * InputManager.Manager.AimingJoystick.Horizontal).normalized;
        if (aimingDirection.magnitude < 0.05)
            return new WanderingPlayerState();

        scientist.Animator.SetFloat("magazineLeft", scientist.HandHolster.Weapon.MagazineLeft);
        scientist.Animator.SetFloat("capacityLeft", scientist.HandHolster.Weapon.CapacityLeft);

        Quaternion targetRotation = Quaternion.LookRotation(aimingDirection);
        scientist.transform.rotation = Quaternion.Slerp(scientist.transform.rotation, targetRotation, Time.deltaTime * scientist.RotationSpeed);
        movementVelocity = (Vector3.forward * InputManager.Manager.MovementJoystick.Vertical + Vector3.right * InputManager.Manager.MovementJoystick.Horizontal).magnitude;
        movementDirection = (Vector3.forward * InputManager.Manager.MovementJoystick.Vertical + Vector3.right * InputManager.Manager.MovementJoystick.Horizontal).normalized;
       
        setAnimatorRunningDirection(movementDirection, aimingDirection);

        return this;
    }

    private void setAnimatorRunningDirection(Vector3 movementDirection, Vector3 aimingDirection)
    {

        float angle = Vector3.SignedAngle(movementDirection, aimingDirection, Vector3.up);
        scientist.Animator.SetBool("forwardRun", false);
        scientist.Animator.SetBool("strafeLeftRun", false);
        scientist.Animator.SetBool("backwardRun", false);
        scientist.Animator.SetBool("strafeRightRun", false);

        if (angle < 50 && angle > -50)
            scientist.Animator.SetBool("forwardRun", true);
        else if(angle >= 50  && angle < 130)
            scientist.Animator.SetBool("strafeLeftRun", true);
        else if (angle >= 130 || angle < -130)
            scientist.Animator.SetBool("backwardRun", true);
        else if(angle >= -130 && angle <= -50)
            scientist.Animator.SetBool("strafeRightRun", true);

    }

    public void NeedsToReload()
    {
        scientist.Animator.SetTrigger("ReloadWeapon");
    }
}
