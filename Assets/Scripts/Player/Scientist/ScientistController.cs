﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScientistController : MonoBehaviour, IDamageable
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private Holster backHolster;
    [SerializeField] private Holster handHolster;
    [SerializeField] private LayerMask enemyMask;
    private Animator animator;

    private CharacterController controller;


    private const float gravity = -9.81f;

    private Vector3 movementDirection;
    private Vector3 aimingDirection;
    private Vector3 vvelocity;
    private bool isAiming;

    private static ScientistController instance;

    public static ScientistController Instance { get => instance; }

    public float MovementSpeed { get => movementSpeed;}
    public float RotationSpeed { get => rotationSpeed;}
    public Holster BackHolster { get => backHolster; }
    public Holster HandHolster { get => handHolster;}
    public CharacterController Controller { get => controller;}
    public Animator Animator { get => animator;}
    public APlayerWeapon HoldingWeapon { get => handHolster.Weapon; }
    public LayerMask EnemyMask { get => enemyMask;}

    private IPlayerState playerState;

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
            controller = GetComponent<CharacterController>();
            animator = GetComponent<Animator>();
            playerState = new WanderingPlayerState();
            playerState.Enter(this);
        }

    }

    private void Start()
    {
        InputManager.Manager.SwitchWeaponButton.onClick.AddListener(OnPressSwitchWeapon);
    }

    private void Update()
    {
        animator.SetBool("grounded", controller.isGrounded);
        IPlayerState ps = playerState.Update();
        if(ps!= playerState)
        {
            playerState.Exit();
            this.playerState = ps;
            ps.Enter(this);
            ps.Update();
        }

    }

    private void FixedUpdate()
    {
        playerState.FixedUpdate();
    }

    public void Shoot()
    {
        if(!handHolster.IsEmpty())
            handHolster.Weapon.Shoot(null);
    }

    public void Reload()
    {
        if (!handHolster.IsEmpty())
        {
            handHolster.Weapon.Reload();
            AudioManager.Instance.PlayAmmoRifleReload();
        }
           
    }

    public void SwitchWeapon()
    {
        APlayerWeapon handWeapon = handHolster.Weapon;
        APlayerWeapon backWeapon = backHolster.Weapon;
        handHolster.Hold(backWeapon);
        backHolster.Hold(handWeapon);
    }

    public void OnPressSwitchWeapon()
    {
        animator.SetTrigger("SwitchWeapon");
    }

    public void PickWeapon(APlayerWeapon weapon)
    {
        AudioManager.Instance.PlayWeaponPickupSound();
        handHolster.Hold(weapon);
    }

    public void takeDamage(float damage)
    {
        if (damage <= 0f)
            return;
        float energyLost = EnergyManager.Manager.LoseEnergy(EnergyType.Health, damage);
        if(energyLost <= 0)
        {
            this.gameObject.SetActive(false);
            //aggiungere animazione morte (?)
            EventsManager.Manager.gameOver.Invoke();
        }
    }

    public bool HasDied()
    {
        return EnergyManager.Manager.Amount(EnergyType.Health) <= 0f;
    }

    public void Stop()
    {
        controller.SimpleMove(Vector3.zero);
    }
}
