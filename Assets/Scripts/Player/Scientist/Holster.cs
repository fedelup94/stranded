﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HolsterType
{
    Back,
    Hand
}

[System.Serializable]
public class Holster : MonoBehaviour
{
    [SerializeField] private APlayerWeapon weapon;
    [SerializeField] private HolsterType holsterType;

    public APlayerWeapon Weapon { get => weapon;}

    public bool IsEmpty()
    {
        return weapon == null;
    }

    public void Hold(APlayerWeapon weapon)
    {
        if(weapon != null)
        {
            weapon.Position(this.transform);
            if (this.holsterType == HolsterType.Back)
                weapon.OnStopAiming();
        }

        this.weapon = weapon;
        EventsManager.Manager.holsterWeaponChanged.Invoke(holsterType, weapon); 
    }
}
