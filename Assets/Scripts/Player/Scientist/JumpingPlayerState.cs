﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingPlayerState : IPlayerState
{
    private ScientistController scientist;
    private Vector3 horizontalVelocity;
    private float jumpSpeed = 5.5f;
    private Vector3 jumpVelocity = Vector3.zero;
    private const float gravity = -9.8f;


    public void Enter(ScientistController scientist)
    {
        this.scientist = scientist;
        horizontalVelocity = scientist.Controller.velocity;
        jumpVelocity.y = jumpSpeed;
    }

    public void Exit()
    {
        scientist.Animator.SetFloat("v_velocity", 0f);
    }

    public IPlayerState FixedUpdate()
    {
        scientist.Controller.Move(horizontalVelocity * Time.deltaTime);
        jumpVelocity.y += gravity * Time.deltaTime;
        scientist.Controller.Move(jumpVelocity * Time.deltaTime);
        scientist.Animator.SetFloat("v_velocity", scientist.Controller.velocity.y);
        return this;
    }

    public IPlayerState Update()
    {
        if (jumpVelocity.y <= 0 && scientist.Controller.isGrounded)
            return new WanderingPlayerState();
        else
            return this;
    }
}
