﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingPlayerState : IPlayerState
{
    private ScientistController scientist;
    private Vector3 movementDirection;
    private float movementVelocity;


    public void Enter(ScientistController scientist)
    {
        this.scientist = scientist;
        movementDirection = Vector3.zero;
    }

    public IPlayerState Update()
    {
       // if (InputManager.Manager.JumpButton.ButtonPressed)
           // return new JumpingPlayerState();

        Vector3 aimingDirection = (Vector3.forward * InputManager.Manager.AimingJoystick.Vertical + Vector3.right * InputManager.Manager.AimingJoystick.Horizontal).normalized;
        if (aimingDirection.magnitude > 0.05 && !scientist.HandHolster.IsEmpty())
            return new FiringPlayerState();

        movementVelocity = (Vector3.forward * InputManager.Manager.MovementJoystick.Vertical + Vector3.right * InputManager.Manager.MovementJoystick.Horizontal).magnitude;
        movementDirection = (Vector3.forward * InputManager.Manager.MovementJoystick.Vertical + Vector3.right * InputManager.Manager.MovementJoystick.Horizontal).normalized;
        if (movementDirection.magnitude > 0.05)
        {
            Quaternion targetRotation = Quaternion.LookRotation(movementDirection);
            scientist.transform.rotation = Quaternion.Slerp(scientist.transform.rotation, targetRotation, Time.deltaTime * scientist.RotationSpeed);
        }
        return this;
    }

    public IPlayerState FixedUpdate()
    {
        Vector3 velocity = movementDirection * movementVelocity * scientist.MovementSpeed;
        scientist.Animator.SetFloat("velocity", movementVelocity);
        scientist.Controller.SimpleMove(velocity);
        return this;
    }

    public void Exit()
    {
    }
}
