﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletCounter : MonoBehaviour
{
    [SerializeField] private Text text;

    // Start is called before the first frame update

    // Update is called once per frame
    public void SetBulletsLeft(int bulletsLeft)
    {
        text.text = "" + bulletsLeft;
    }
}
