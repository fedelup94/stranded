﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    IPickable pickableObject;
    [SerializeField] GameObject pickableObjectPrefab;

    private void Awake()
    {
        GameObject pickableObjectInstance = Instantiate(pickableObjectPrefab, this.transform.position, this.transform.rotation);
        pickableObject = pickableObjectInstance.GetComponent<IPickable>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ScientistController scientist = other.GetComponent<ScientistController>();
            if(scientist != null)
            {
                pickableObject.OnPickUp(scientist);
            }
        }

        Destroy(this);
           
    }
}
