﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefillableTarget : MonoBehaviour
{
    private Refillable refillable;

    public Refillable Refillable { get => refillable; }

    private void Start()
    {
        InputManager.Manager.RefillButton.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        Refillable refillable = other.gameObject.GetComponent<Refillable>();
        if (refillable != null)
        {
            InputManager.Manager.RefillButton.gameObject.SetActive(true);
            this.refillable = refillable;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Refillable refillable = other.gameObject.GetComponent<Refillable>();
        if (refillable != null)
        {
            if (refillable == this.refillable)
            {
                this.refillable = null;
                InputManager.Manager.RefillButton.gameObject.SetActive(false);
            }

        }
    }

    private void OnDisable()
    {
        if (InputManager.Manager != null && InputManager.Manager.RefillButton != null)
        {
            InputManager.Manager.RefillButton.gameObject.SetActive(false);
        }
        if(refillable != null)
            refillable.StopRefilling();
    }

    // Update is called once per frame
    void Update()
    {
        if(InputManager.Manager.RefillButton.ButtonFirstPressed)
        {
            //AudioManager.Instance.PlayTowerRechargeSound();
            refillable.StartRefilling();
        }
        else if(InputManager.Manager.RefillButton.ButtonFirstReleased)
        {
            refillable.StopRefilling();
        }
    }
}
