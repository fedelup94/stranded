﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonActivationController : MonoBehaviour
{
    [SerializeField] Color activeColor;
    [SerializeField] Color inactiveColor;
    [SerializeField] List<Image> images;
    [SerializeField] TextMeshProUGUI text;
    [SerializeField] Button button;
    [SerializeField] BuildableTool buildableTool;

    public void Start()
    {
        OnMoneyValueChanged(EnergyType.Money, EnergyManager.Manager.Amount(EnergyType.Money));
        EventsManager.Manager.energyLeft.AddListener(OnMoneyValueChanged);
    }

    public void OnEnable()
    {
        if (EnergyManager.Manager == null)
            return;
        OnMoneyValueChanged(EnergyType.Money, EnergyManager.Manager.Amount(EnergyType.Money));
    }

    public void Disable()
    {
        for(int i = 0; i < images.Count; i++)
        {
            images[i].color = new Color(inactiveColor.r, inactiveColor.g, inactiveColor.b, images[i].color.a);
        }
        text.color = new Color(inactiveColor.r, inactiveColor.g, inactiveColor.b, text.color.a);
        button.interactable = false;
    }

    public void Enable()
    {
        for (int i = 0; i < images.Count; i++)
        {
            images[i].color = new Color(activeColor.r, activeColor.g, activeColor.b, images[i].color.a);
        }
        text.color = new Color(activeColor.r, activeColor.g, activeColor.b, text.color.a);
        button.interactable = true;
    }

    public void OnMoneyValueChanged(EnergyType energyType, float moneyValue)
    {
        if (energyType != EnergyType.Money)
            return;
        if (moneyValue >= buildableTool.MoneyCost)
            Enable();
        else
            Disable();

    }

}
