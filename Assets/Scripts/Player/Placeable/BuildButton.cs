﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildButton : MonoBehaviour
{
    [SerializeField] private Transform scientistBuildPosition;
    [SerializeField] private GameObject objectToBuild;
    // Start is called before the first frame update

    public void Build()
    {
       GameObject go = Instantiate(objectToBuild, scientistBuildPosition.position, scientistBuildPosition.rotation);
        go.transform.parent = scientistBuildPosition;
    }
}
