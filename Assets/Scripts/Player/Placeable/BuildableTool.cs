﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildableTool : MonoBehaviour
{
    [SerializeField] GameObject buildableObject;
    [SerializeField] PlaceArea buildArea;
    [SerializeField] Image backImage;
    [SerializeField] private TextMeshProUGUI costText;
    [SerializeField] private float moneyCost;


    APlaceable aPlaceable;

    public APlaceable Placeable { get => aPlaceable;}
    public float MoneyCost { get => moneyCost;}

    public void Awake()
    {
        InstantiatePlaceable();
        HidePlaceable();
        //costText.text = "$ " + ((int)moneyCost).ToString();
        costText.text = ((int)moneyCost).ToString();
    }

    public void Start()
    {
        EventsManager.Manager.startPlaceObject.AddListener(IsPlacingAnotherObject);
        EventsManager.Manager.hasPlaceObject.AddListener(ReplacePlaceable);
    }


    public void IsPlacingAnotherObject(APlaceable aPlaceable)
    {
        if (aPlaceable == this.aPlaceable)
            return;
        HidePlaceable();
    }

    public void ToggleBuildButton()
    {
        if (aPlaceable.gameObject.activeInHierarchy)
        {
            EventsManager.Manager.stopPlaceObject.Invoke(aPlaceable);
            HidePlaceable();
        }
        else
        {
            //L'utente vuole piazzare un oggetto, però prima devo controllare che il relativo costo non superi l'ammontare di monete a disposizione del giocatore
            if (moneyCost > EnergyManager.Manager.Amount(EnergyType.Money))
                return;
            ActivatePlaceable();
            EventsManager.Manager.startPlaceObject.Invoke(aPlaceable);
        }
    }

    private void InstantiatePlaceable()
    {
        GameObject instantiatedGO = Instantiate(buildableObject);
        aPlaceable = instantiatedGO.GetComponent<APlaceable>();

    }

    private void HidePlaceable()
    {
        aPlaceable.gameObject.SetActive(false);
        backImage.enabled = false;
    }

    private void ActivatePlaceable()
    {
        aPlaceable.gameObject.SetActive(true);
        backImage.enabled = true;
    }

    public void ReplacePlaceable(APlaceable aPlaceable)
    {
        if(this.aPlaceable == aPlaceable)
        {
            EnergyManager.Manager.LoseEnergy(EnergyType.Money, moneyCost);
            InstantiatePlaceable();
            HidePlaceable();
        }
    }

    public void OnMoneyValueChanged(EnergyType energyType, float moneyValue)
    {
        if (energyType != EnergyType.Money)
            return;

    }
}
