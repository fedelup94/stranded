﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRefillable 
{
    bool Refill(float energyAmount);
    bool IsFull();
    bool IsEmpty();
}
