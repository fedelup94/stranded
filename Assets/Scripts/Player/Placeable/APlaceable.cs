﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class APlaceable: MonoBehaviour
{
    [SerializeField] protected List<Collider> collidersToAvoid;
    public List<Collider> CollidersToAvoid { get => collidersToAvoid; }

    public void StartMovingWith(Transform parentTransform)
    {
        this.gameObject.transform.position = parentTransform.position;
        this.gameObject.transform.parent = parentTransform;
        OnStartMove();

    }

    protected abstract void OnStartMove();

    public void Place()
    {
        AudioManager.Instance.PlayTowerPlaceSound();
        this.gameObject.transform.parent = null;
        OnStopMove();
    }

    protected abstract void OnStopMove();
}
