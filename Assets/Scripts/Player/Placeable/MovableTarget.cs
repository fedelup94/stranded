﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableTarget : MonoBehaviour
{
    private APlaceable placeable;

    public APlaceable Placeable { get => placeable; }

    private void Start()
    {
        InputManager.Manager.MoveButton.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        APlaceable placeable = other.gameObject.GetComponent<APlaceable>();
        if (placeable != null)
        {
            InputManager.Manager.MoveButton.gameObject.SetActive(true);
            this.placeable = placeable;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        APlaceable placeable = other.gameObject.GetComponent<APlaceable>();
        if (placeable != null)
        {
            if (placeable == this.placeable)
            {
                this.placeable = null;
                InputManager.Manager.MoveButton.gameObject.SetActive(false);
            }

        }
    }

    private void OnDisable()
    {
        if (InputManager.Manager != null && InputManager.Manager.MoveButton != null)
        {
            InputManager.Manager.MoveButton.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (InputManager.Manager.MoveButton.ButtonFirstPressed)
        {
            if (placeable == null)
                return;
            AudioManager.Instance.PlayTowerPickupSound();
            EventsManager.Manager.startPlaceObject.Invoke(placeable);
        }
    }
}
