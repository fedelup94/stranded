﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(IRefillable))]
public class Refillable : MonoBehaviour
{
    [SerializeField] protected float refillableRate;
    [SerializeField] protected EnergyType energyType;
    [SerializeField] private float refillEveryTotSec = 1f;
    private IEnumerator refillCoroutine;
    private IRefillable refillable;


    private void Awake()
    {
        refillable = GetComponent<IRefillable>();
    }

    private IEnumerator RefillCoroutine()
    {
        float refillAmountObtained = float.MaxValue;
        bool hasReachedMaxAmount = false;
        while(refillAmountObtained > 0 && !hasReachedMaxAmount)
        {
            refillAmountObtained = EnergyManager.Manager.LoseEnergy(this.energyType, this.refillableRate * this.refillEveryTotSec);
            hasReachedMaxAmount = refillable.Refill(refillAmountObtained);
            yield return new WaitForSeconds(refillEveryTotSec);
        }
    }

    public void StartRefilling()
    {
        if (refillable.IsFull())
            return;
        if(refillCoroutine != null)
        {
            StopRefilling();
        }
        refillCoroutine = RefillCoroutine();
        StartCoroutine(refillCoroutine);
    }

    public void StopRefilling()
    {
        if(refillCoroutine != null)
        {
            Debug.Log("Stopping coroutine");
            StopCoroutine(refillCoroutine);
            refillCoroutine = null;
        }
    }
}
