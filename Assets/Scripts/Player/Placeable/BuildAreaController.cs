﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildAreaController : MonoBehaviour
{
    [SerializeField] private BuildAreaSpeed buildAreaSpeed;
    Vector3 movementDirection;
    bool playerHasControl = true;


    // Update is called once per frame
    void FixedUpdate()
    {
        if(playerHasControl)
        {
            //Debug.Log(InputManager.Manager.BuildMovementJoystick.Vertical);
            movementDirection = (Vector3.forward * InputManager.Manager.BuildMovementJoystick.Vertical + Vector3.right * InputManager.Manager.BuildMovementJoystick.Horizontal);  
        }
        transform.Translate(movementDirection * Time.fixedDeltaTime * buildAreaSpeed.Speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MapLimit"))
        {
            playerHasControl = false;
            if (movementDirection.magnitude <= 0f)
            {
                movementDirection = (other.gameObject.transform.position - this.transform.position).normalized;
                movementDirection.y = 0f;
            }
               
            movementDirection = -movementDirection;
        }
         
    }

    private Vector3 GetRayCastDirection()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 2f, LayerMask.NameToLayer("Default")))
        {
            if (hit.collider.gameObject.CompareTag("MapLimit"))
                return transform.TransformDirection(Vector3.forward);
        }
        else if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), out hit, 2f, LayerMask.NameToLayer("Default")))
        {
            if (hit.collider.gameObject.CompareTag("MapLimit"))
                return transform.TransformDirection(Vector3.back);
        }
        else if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, 2f, LayerMask.NameToLayer("Default")))
        {
            if (hit.collider.gameObject.CompareTag("MapLimit"))
                return transform.TransformDirection(Vector3.right);
        }
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out hit, 2f, LayerMask.NameToLayer("Default")))
        {
            if (hit.collider.gameObject.CompareTag("MapLimit"))
                return transform.TransformDirection(Vector3.left);
        }

        return Vector3.zero;

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MapLimit"))
        {
            playerHasControl = true;
            movementDirection = Vector3.zero;
        }
    }


}
