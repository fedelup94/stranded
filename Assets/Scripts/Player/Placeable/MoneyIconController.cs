﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MoneyIconController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI moneyText;
    // Start is called before the first frame update
    void Start()
    {
        EventsManager.Manager.energyLeft.AddListener(OnMoneyValueChanged);
        moneyText.text = ((int)EnergyManager.Manager.Amount(EnergyType.Money)).ToString();

    }

    public void OnMoneyValueChanged(EnergyType energyType, float value)
    {
        if (energyType == EnergyType.Money)
            moneyText.text = ((int)value).ToString();
    }
}
