﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceArea : MonoBehaviour
{
    [SerializeField] PlaceableTarget placeableTarget;
    [SerializeField] PulsingColor pulsingColor;
    APlaceable aPlaceable;

    private static PlaceArea instance;
    private bool canPlace = true;
    private List<GameObject> obstacles;
    private List<Collider> collidersToAvoid;

    public static PlaceArea Instance { get => instance;}

    private void Awake()
    {
        if(instance == null)
        {
            obstacles = new List<GameObject>();
            collidersToAvoid = new List<Collider>();
            pulsingColor = GetComponent<PulsingColor>();
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        EventsManager.Manager.startPlaceObject.AddListener(StartPlacing);
        EventsManager.Manager.stopPlaceObject.AddListener(StopPlacing);

        InputManager.Manager.PlaceButton.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        obstacles = new List<GameObject>();
    }

    public void StartPlacing(APlaceable aPlaceable)
    {
        if(this.aPlaceable != null)
        {
            this.aPlaceable.Place();
            collidersToAvoid = new List<Collider>();
            for (int i = 0; i < aPlaceable.CollidersToAvoid.Count; i++)
                OnTriggerEnter(aPlaceable.CollidersToAvoid[i]);
        }
        this.aPlaceable = aPlaceable;
        CleanObstacles();
        aPlaceable.StartMovingWith(this.transform);
        collidersToAvoid = aPlaceable.CollidersToAvoid;
        for (int i = 0; i < collidersToAvoid.Count; i++)
            if (obstacles.Contains(collidersToAvoid[i].gameObject))
                obstacles.Remove(collidersToAvoid[i].gameObject);
        Debug.Log("NUM OBSTACLES: " + obstacles.Count);
        Debug.Log("OBSTACLES:");
        foreach (GameObject obstacle in obstacles)
            print(obstacle);
        if (obstacles.Count > 0 && aPlaceable != null)
            pulsingColor.SetColors(Color.red);
        else
            pulsingColor.SetColors(Color.green);
        placeableTarget.gameObject.SetActive(false);
        InputManager.Manager.PlaceButton.gameObject.SetActive(true);
    }

    public void StopPlacing(APlaceable aPlaceable)
    {
        if (aPlaceable == this.aPlaceable)
        {
            this.aPlaceable = null;
            placeableTarget.gameObject.SetActive(true);
            InputManager.Manager.PlaceButton.gameObject.SetActive(false);
        }

    }

    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject);
        if (other.transform.parent == this.transform)
            return;
        if (collidersToAvoid.Contains(other))
            return;

        if (other.gameObject != null && !obstacles.Contains(other.gameObject))
            obstacles.Add(other.gameObject);
        CleanObstacles();

        if (obstacles.Count > 0 && aPlaceable != null)
            pulsingColor.SetColors(Color.red);

        Debug.Log("NUM OBSTACLES: " + obstacles.Count);
    }

    public void OnTriggerExit(Collider other)
    {
        obstacles.Remove(other.gameObject);

        if (obstacles.Count <= 0)
            pulsingColor.SetColors(Color.green);
        CleanObstacles();
    }

    private void OnDisable()
    {
        obstacles = new List<GameObject>();
    }

    private void CleanObstacles()
    {
        int count = obstacles.Count;
        for(int i = 0; i < count; i++)
        {
            if(obstacles[i] == null || !obstacles[i].activeInHierarchy)
            {
                obstacles.Remove(obstacles[i]);
                count--;
            }
        }
    }



    private void Update()
    {
        //Debug.Log("NUM OBSTACLES: " + obstacles.Count);
        if (InputManager.Manager.PlaceButton.ButtonFirstPressed)
        {
            if (obstacles.Count > 0f)
                return;
            if (aPlaceable != null)
            {
                aPlaceable.Place();
                CleanObstacles();
                collidersToAvoid = new List<Collider>();
                for (int i = 0; i < aPlaceable.CollidersToAvoid.Count; i++)
                    OnTriggerEnter(aPlaceable.CollidersToAvoid[i]);
                EventsManager.Manager.hasPlaceObject.Invoke(aPlaceable);
                this.aPlaceable = null;
                pulsingColor.SetColors(Color.green);
                placeableTarget.gameObject.SetActive(true);
                InputManager.Manager.PlaceButton.gameObject.SetActive(false);
            }
        }

    }
}