﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableTarget : MonoBehaviour
{
    private APlaceable aPlaceable;

    public APlaceable APlaceable { get => aPlaceable; }

    private void Start()
    {
        InputManager.Manager.MoveButton.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        APlaceable aPlaceable = other.gameObject.GetComponent<APlaceable>();
        if (aPlaceable != null)
        {
            InputManager.Manager.MoveButton.gameObject.SetActive(true);
            this.aPlaceable = aPlaceable;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        APlaceable aPlaceable = other.gameObject.GetComponent<APlaceable>();
        if (aPlaceable != null)
        {
            if (aPlaceable == this.aPlaceable)
            {
                this.aPlaceable = null;
                InputManager.Manager.MoveButton.gameObject.SetActive(false);
            }
     
        }
    }

    private void OnDisable()
    {
        if(InputManager.Manager != null && InputManager.Manager.MoveButton != null)
            InputManager.Manager.MoveButton.gameObject.SetActive(false);
    }
}
