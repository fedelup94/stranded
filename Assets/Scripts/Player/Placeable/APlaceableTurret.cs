﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

public class APlaceableTurret : APlaceable
{
    private BehaviorTree behaviorTree;
    private void Awake()
    {
        behaviorTree = GetComponent<BehaviorTree>();
    }
    protected override void OnStartMove()
    {
        behaviorTree.enabled = false;
    }
  

    protected override void OnStopMove()
    {
        behaviorTree.enabled = true;
    }
}
