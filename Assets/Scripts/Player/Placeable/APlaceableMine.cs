﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APlaceableMine : APlaceable
{
    Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    protected override void OnStartMove()
    {
        rb.useGravity = false;
    }

    protected override void OnStopMove()
    {
        rb.useGravity = true;
    }
}
