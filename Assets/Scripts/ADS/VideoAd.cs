﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;


public class VideoAd : MonoBehaviour
{
    private static VideoAd instance;

    public static VideoAd Instance { get => instance; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }



    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayVideoAd()
    {
        StartCoroutine(ShowAdWhenReady());
    }

    private IEnumerator ShowAdWhenReady()
    {
        while (!Advertisement.isInitialized)
        {
            yield return new WaitForSeconds(.5f);
        }
        Advertisement.Show();
    }
}
