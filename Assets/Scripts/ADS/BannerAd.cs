﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class BannerAd : MonoBehaviour
{

    public static string placementId = "MenuBanner";


    // Start is called before the first frame update
    void Start()
    {
        Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
        StartCoroutine(ShowBannerWhenReady());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.isInitialized)
        {
            yield return new WaitForSeconds(.5f);
        }
        while (!Advertisement.IsReady("MenuBanner"))
        {
            yield return new WaitForSeconds(.5f);
        }
        Advertisement.Banner.Show("MenuBanner");

    }

    private void OnDestroy()
    {
        Advertisement.Banner.Hide();
    }
}
