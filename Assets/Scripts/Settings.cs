﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Advertisements;


public class Settings : MonoBehaviour
{

    private string gameId = "3583407";
    public bool adsTestMode = true;

    public enum Language { ITALIANO, ENGLISH };

    private static Settings instance;

    private Language language;
    private bool isAudioOn;
    private Lean.Localization.LeanLocalization languageManager;
    //public UnityEvent audioSettingsChange;

    public static Settings Instance { get => instance; }
    public bool IsAudioOn { get => isAudioOn; private set => isAudioOn = value; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
        {
            instance = this;
            languageManager = GetComponent<Lean.Localization.LeanLocalization>();
            IsAudioOn = true;
            InitAds();
        }
    }

    public void NextLanguage()
    {
        switch (language)
        {
            case Language.ITALIANO:
                language = Language.ENGLISH;
                languageManager.SetCurrentLanguage("English");
                break;
            case Language.ENGLISH:
                language = Language.ITALIANO;
                languageManager.SetCurrentLanguage("Italian");
                break;
        }
    }

    public void ToggleAudio()
    {
        isAudioOn = !isAudioOn;
        AudioEnabledCheck.Instance.CheckSettingsOn();
        //audioSettingsChange.Invoke();
    }
    

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void InitAds()
    {
        if (!Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, adsTestMode);
        }
    }

    //private IEnumerator ShowBannerWhenReady()
    //{
    //    while (!Advertisement.IsReady("MenuBanner"))
    //    {
    //        yield return new WaitForSeconds(.5f);
    //    }
    //    Advertisement.Banner.Show("MenuBanner");

    //}

    // Update is called once per frame
    void Update()
    {
        
    }
}
