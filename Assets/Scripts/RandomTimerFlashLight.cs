﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTimerFlashLight : MonoBehaviour
{

    private float timer;
    private new Light light;

    // Start is called before the first frame update
    void Start()
    {
        light = GetComponent<Light>();
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0)
        {
            timer = Random.Range(0f, 4f);
            if (Random.Range(0, 100) % 2 == 0)
                light.enabled = false;
            else
                light.enabled = true;
        }
        timer -= Time.deltaTime;
    }

}
